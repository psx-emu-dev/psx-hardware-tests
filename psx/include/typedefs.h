#ifndef _TYPEDEFS_H_
#define _TYPEDEFS_H_

typedef unsigned char   uint8_t;
typedef unsigned short  uint16_t;
typedef unsigned int    uint32_t;
typedef signed char     int8_t;
typedef signed short    int16_t;
typedef signed int      int32_t;
typedef _Bool           bool;
typedef unsigned int    size_t;

#endif /* _TYPEDEFS_H_ */
