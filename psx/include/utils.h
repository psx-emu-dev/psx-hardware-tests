#ifndef _UTILS_H_
#define _UTILS_H_

#include "typedefs.h"

#define ARRAY_SIZE(_a) (sizeof(_a) / sizeof(*(_a)))

#define NULL ((void*)0)

#define true  1
#define false 0

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

static inline void nop(void) {
  asm volatile("nop");
}

static inline void brk(void) {
  asm volatile("break");
}

static inline void busy_loop(unsigned count) {
  while (count--) {
    nop();
  }
}

static inline uint32_t read32(size_t addr) {
  volatile const uint32_t *p = (volatile void *)addr;

  return *p;
}

static inline uint16_t read16(size_t addr) {
  volatile const uint16_t *p = (volatile void *)addr;

  return *p;
}

static inline uint8_t read8(size_t addr) {
  volatile const uint8_t *p = (volatile void *)addr;

  return *p;
}

static inline void write32(size_t addr, uint32_t val) {
  asm volatile("sw %0, 0(%1)" : : "r" (val), "r" (addr));
}

static inline void write16(size_t addr, uint32_t val) {
  asm volatile("sh %0, 0(%1)" : : "r" (val), "r" (addr));
}

static inline void write8(size_t addr, uint32_t val) {
  asm volatile("sb %0, 0(%1)" : : "r" (val), "r" (addr));
}

void __attribute__((noinline)) enter_kseg0();
void __attribute__((noinline)) enter_kseg1();

#endif /* _UTILS_H_ */
