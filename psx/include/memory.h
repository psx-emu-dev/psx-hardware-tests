#ifndef _MEMORY_H_
#define _MEMORY_H_

/* Joypad/Memory Card controller base */
#define JOY_MC_BASE            0x1f801040UL

/* MDEC base */
#define MDEC_BASE              0x1f801820UL

/* SPU base */
#define SPU_BASE               0x1f801c00UL

/* Timer base */
#define TIMER_BASE             0x1f801100UL

/* CD Controller base */
#define CD_BASE                0x1f801800UL

#endif /* _MEMORY_H_ */
