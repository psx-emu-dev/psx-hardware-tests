#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_

#include "typedefs.h"

void exception_handler();

void load_exception_handler(uint32_t address, uint32_t *buffer);
void save_exception_handler(uint32_t address, uint32_t *buffer);

#endif  // _EXCEPTION_H_
