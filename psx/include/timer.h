#ifndef _TIMER_H_
#define _TIMER_H_

#include "memory.h"
#include "utils.h"

static inline uint32_t timer_0_value() {
  return read16(TIMER_BASE);
}

static inline void timer_0_init() {
  write16(TIMER_BASE + 4, 0);
}

#endif /* _TIMER_H_ */
