#ifndef _GPU_H_
#define _GPU_H_

#include "utils.h"

typedef struct {
  uint8_t r;
  uint8_t g;
  uint8_t b;
} gpu_color_t;

typedef struct {
  int16_t x;
  int16_t y;
} gpu_offset_t;

typedef struct {
  uint16_t x;
  uint16_t y;
} gpu_point_t;

typedef struct {
  uint16_t width;
  uint16_t height;
} gpu_size_t;

#define GPU_GP0  0x1f801810
#define GPU_GP1  0x1f801814
#define GPU_READ 0x1f801810
#define GPU_STAT 0x1f801814

#define GPU_READY_CMD (1 << 26)
#define GPU_READY_TX  (1 << 27)

uint32_t gpu_read_stat();
uint32_t gpu_read_data();
void gpu_gp0(uint32_t data);
void gpu_gp1(uint32_t data);
void gpu_wait_until_ready(uint32_t mask);
void gpu_clear_vram();

/* GP1 commands */

void gpu_gp1_00_reset();

/* GP0 commands */

void gpu_gp0_set_drawing_area_tl(gpu_point_t tl);
void gpu_gp0_set_drawing_area_br(gpu_point_t br);
void gpu_gp0_set_drawing_offset(gpu_offset_t offset);
void gpu_gp0_02_fill_area(gpu_point_t tl, gpu_size_t size, gpu_color_t color);
void gpu_gp0_40_draw_line(gpu_point_t a, gpu_point_t b, gpu_color_t color);
void gpu_gp0_50_draw_line(gpu_point_t a, gpu_color_t color_a, gpu_point_t b, gpu_color_t color_b);
void gpu_gp0_c0_copy(gpu_point_t top_left, gpu_size_t size);

#endif /* _GPU_H_ */
