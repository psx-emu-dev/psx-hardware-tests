#ifndef _BIOS_H_
#define _BIOS_H_

#include "typedefs.h"

void *bios_memset(void *str, int c, size_t n);
void *bios_malloc(size_t);
void bios_free(void *);
void bios_init_heap(void *, size_t);
int bios_printf(const char *, ...) __attribute__ ((format (printf, 1, 2)));
void bios_flush_cache();

#endif /* _BIOS_H_ */
