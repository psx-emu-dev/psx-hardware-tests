#ifndef _PSX_TEST_H_
#define _PSX_TEST_H_

#include "bios.h"

#define LOG(_f, _fmt, ...) \
  bios_printf(_f "%5u: " _fmt "\n", __LINE__ , ## __VA_ARGS__)

#define ERROR(...) LOG("!", "[ERROR] " __VA_ARGS__)
#define INFO(...) LOG(".", __VA_ARGS__)

#define ASSERT_EQ(expect, actual, msg) do { \
  if ((expect) != (actual)) { \
    ERROR("Assertion failed."); \
    ERROR(msg, expect, actual); \
  } } while (0)

#define TEST_ASSERT_EQ(expect, actual, msg) do { \
  if ((expect) != (actual)) { \
    __test_result = false; \
    ERROR("Assertion failed."); \
    ERROR(msg, expect, actual); \
    goto __test_finish; \
  } } while (0)

#define TEST_EXPECT_EQ(expect, actual, msg) do { \
  if ((expect) != (actual)) { \
    __test_result = false; \
    ERROR("Expectation failed."); \
    ERROR(msg, expect, actual); \
  } } while (0)

#define TEST(name, body) \
  static bool __test_##name() { \
    bool __test_result = true; \
    INFO("Running test '" #name "'.."); \
    body \
  __test_finish: \
    if (__test_result) { \
      INFO("Test '\x1b[32m" #name "\x1b[37m' passed."); \
    } \
    else { \
      INFO("Test '\x1b[31m" #name "\x1b[37m' failed."); \
    } \
    return __test_result; \
  }

#endif /* _PSX_TEST_H_ */
