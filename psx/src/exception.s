.include "common.s"

.set mips1
.set noat
.set noreorder

.global exception_handler
.type exception_handler, function

exception_handler:
    la $k0, exception_handler_
    jr $k0
    nop

exception_handler_:
    push $at, $v0, $v1, $a0, $a1, $a2, $a3, $t0, $t1, $t2, $t3, $t4, $t5, $t6, $t7, $s0, $s1, $s2, $s3, $s4, $s5, $s6, $s7, $t8, $t9, $gp, $fp, $ra

    la   $a0, msg_header
    jal  bios_printf
    nop

    mfc0 $a1, $6
    la   $a0, msg_tar
    jal  bios_printf
    nop

    mfc0 $a1, $8
    la   $a0, msg_bad_vaddr
    jal  bios_printf
    nop

    mfc0 $a1, $12
    la   $a0, msg_sr
    jal  bios_printf
    nop

    mfc0 $a1, $13
    la   $a0, msg_cause
    jal  bios_printf
    nop

    mfc0 $a1, $14
    la   $a0, msg_epc
    jal  bios_printf
    nop

    pull $at, $v0, $v1, $a0, $a1, $a2, $a3, $t0, $t1, $t2, $t3, $t4, $t5, $t6, $t7, $s0, $s1, $s2, $s3, $s4, $s5, $s6, $s7, $t8, $t9, $gp, $fp, $ra

    mfc0 $k0, $14
    nop
    nop
    addi $k0, 4
    jr   $k0
    rfe


msg_header: .asciiz "An exception occurred.\n"
msg_tar: .asciiz "TAR: %08x\n"
msg_bad_vaddr: .asciiz "BadVAddr: %08x\n"
msg_sr: .asciiz "SR: %08x\n"
msg_cause: .asciiz "Cause: %08x\n"
msg_epc: .asciiz "EPC: %08x\n"
