#include "gpu.h"

uint32_t gpu_read_status() {
  return read32(GPU_STAT);
}

uint32_t gpu_read_data() {
  return read32(GPU_READ);
}

void gpu_gp0(uint32_t data) {
  write32(GPU_GP0, data);
}

void gpu_gp1(uint32_t data) {
  write32(GPU_GP1, data);
}

void gpu_wait_until_ready(uint32_t mask) {
  while ((gpu_read_status() & mask) == 0) {
    nop();
  }
}

void gpu_clear_vram() {
  gpu_point_t point = { 0, 0 };
  gpu_size_t size = { 0x3f0, 0x1ff };
  gpu_color_t color = { 0, 0, 0 };
  gpu_gp0_02_fill_area(point, size, color);
}

#define mk_bgr24(c)  \
  ((c).b << 16) | \
  ((c).g <<  8) | \
  ((c).r <<  0)

#define mk_command(c) ((c) << 24)

#define mk_param(h, l) (((h) << 16) | (l))

/* GP1 commands */

void gpu_gp1_00_reset() {
  gpu_wait_until_ready(GPU_READY_CMD);

  gpu_gp1(mk_command(0x00));
}

/* GP0 commands */

void gpu_gp0_set_drawing_area_tl(gpu_point_t tl) {
  gpu_wait_until_ready(GPU_READY_CMD);
  gpu_gp0(mk_command(0xe3) | (tl.y << 10) | tl.x);
}

void gpu_gp0_set_drawing_area_br(gpu_point_t br) {
  gpu_wait_until_ready(GPU_READY_CMD);
  gpu_gp0(mk_command(0xe4) | (br.y << 10) | br.x);
}

void gpu_gp0_set_drawing_offset(gpu_offset_t offset) {
  gpu_wait_until_ready(GPU_READY_CMD);

  uint32_t cmd = mk_command(0xe5);
  cmd |= (offset.y & 0x7ff) << 11;
  cmd |= (offset.x & 0x7ff);

  gpu_gp0(cmd);
}

void gpu_gp0_02_fill_area(gpu_point_t tl, gpu_size_t size, gpu_color_t color) {
  gpu_wait_until_ready(GPU_READY_CMD);

  gpu_gp0(mk_command(0x02) | mk_bgr24(color));
  gpu_gp0(mk_param(tl.y, tl.x));
  gpu_gp0(mk_param(size.height, size.width));
}

void gpu_gp0_40_draw_line(gpu_point_t a, gpu_point_t b, gpu_color_t color) {
  gpu_wait_until_ready(GPU_READY_CMD);

  gpu_gp0(mk_command(0x40) | mk_bgr24(color));
  gpu_gp0(mk_param(a.y, a.x));
  gpu_gp0(mk_param(b.y, b.x));
}

void gpu_gp0_50_draw_line(
    gpu_point_t a,
    gpu_color_t color_a,
    gpu_point_t b,
    gpu_color_t color_b) {
  gpu_wait_until_ready(GPU_READY_CMD);

  gpu_gp0(mk_bgr24(color_a) | mk_command(0x50));
  gpu_gp0(mk_param(a.y, a.x));
  gpu_gp0(mk_bgr24(color_b));
  gpu_gp0(mk_param(b.y, b.x));
}

void gpu_gp0_c0_copy(gpu_point_t top_left, gpu_size_t size) {
  gpu_wait_until_ready(GPU_READY_CMD);

  gpu_gp0(mk_command(0xc0));
  gpu_gp0(mk_param(top_left.y, top_left.x));
  gpu_gp0(mk_param(size.height, size.width));

  gpu_wait_until_ready(GPU_READY_TX);
}
