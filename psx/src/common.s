.set mips1
.set noreorder

.macro def_reg_list_size sum, reg, regs:vararg
.ifb \regs
    .equ reg_list_size, (\sum + 4)
.else
    def_reg_list_size (\sum + 4), \regs
.endif
.endm

.macro push_one reg ofs
    sw \reg, (\ofs)($sp)
.endm

.macro pull_one reg ofs
    lw \reg, (\ofs)($sp)
.endm

.macro list cmd, ofs, reg, regs:vararg
  \cmd \reg, (\ofs - 4)
.ifnb \regs
  list \cmd, (\ofs - 4), \regs
.endif
.endm

.macro push regs:vararg
    def_reg_list_size 0, \regs
    addi $sp,-reg_list_size
    list push_one, reg_list_size, \regs
    move $fp, $sp
.endm

.macro pull regs:vararg
    move $sp, $fp
    list pull_one, reg_list_size, \regs
    addi $sp, reg_list_size
.endm

.macro println s
    la  $a0, string\@
    jal bios_puts
    nop
.data
    string\@: .asciz "\s\n"
.text
.endm

.macro printfn s
    la  $a0, string\@
    jal bios_printf
    nop
.data
    string\@: .asciz "\s\n"
.text
.endm
