#include "exception.h"
#include "bios.h"
#include "utils.h"

void save_exception_handler(uint32_t address, uint32_t *buffer) {
  buffer[0] = read32(address + 0x0);
  buffer[1] = read32(address + 0x4);
  buffer[2] = read32(address + 0x8);
  buffer[3] = read32(address + 0xC);
}

void load_exception_handler(uint32_t address, uint32_t *buffer) {
  write32(address + 0x0, buffer[0]);
  write32(address + 0x4, buffer[1]);
  write32(address + 0x8, buffer[2]);
  write32(address + 0xC, buffer[3]);
  bios_flush_cache();
}
