.set mips1
.set noreorder
.set noat

.text

.global enter_kseg0
.type enter_kseg0, function

enter_kseg0:
    li  $t0,~0xf0000000
    li  $t1, 0x80000000
    and $ra, $ra, $t0
    or  $ra, $ra, $t1
    jr  $ra
    nop

.global enter_kseg1
.type enter_kseg1, function

enter_kseg1:
    li  $t0,~0xf0000000
    li  $t1, 0xa0000000
    and $ra, $ra, $t0
    or  $ra, $ra, $t1
    jr  $ra
    nop
