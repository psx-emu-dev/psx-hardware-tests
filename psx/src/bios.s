.set mips1
.set noreorder
.set noat

# For creating BIOS invocations

.macro bios dest code name
  .global \name
  .type \name, function

  \name:
    j \dest
    li $t1, \code
.endm

# BIOS invocations

.text

bios 0xa0 0x2b bios_memset
bios 0xa0 0x33 bios_malloc
bios 0xa0 0x34 bios_free
bios 0xa0 0x39 bios_init_heap
bios 0xa0 0x3e bios_puts
bios 0xa0 0x3f bios_printf
bios 0xa0 0x44 bios_flush_cache
