#include "bios.h"

extern int main();

void _start() {
  bios_printf("Running main...\n");
  bios_printf("Application exited with code %d\n", main());
}
