VariantDir('out', 'src', duplicate=0)

from os import listdir, path

env = Environment()

# Configure variables

vars = Variables('.build.conf')
vars.Add('PSX_TOOLCHAIN_PATH', '', '/opt/psx/bin')
vars.Add('PSX_TOOLCHAIN_PREFIX', '', 'mipsel-unknown-elf-')
vars.Update(env)

toolchain_prefix = env.subst('$PSX_TOOLCHAIN_PATH/$PSX_TOOLCHAIN_PREFIX')
toolchain_dir = env.subst('$PSX_TOOLCHAIN_PATH/')

env['AS'] = toolchain_prefix + 'as'
env['AR'] = toolchain_prefix + 'ar'
env['CC'] = toolchain_prefix + 'gcc'
env['LINK'] = toolchain_prefix + 'ld'
env['PROGSUFFIX'] = '.elf'

common_flags = [
  '-mips1',
  '-mno-mips16',
  '-msoft-float',
  '-I', 'psx/include',
  '-I', 'psx/src'
]

env['ASFLAGS'] = common_flags + [
  '--fatal-warnings'
]

env['CCFLAGS'] = common_flags + [
  '-nostdlib',
  '-Wall',
  '-Wextra',
  '-Werror=implicit-function-declaration',
  '-ffunction-sections',
  '-fdata-sections',
  '-O2',
  '-G0'
]

env['LINKFLAGS'] = [
  '-nostdlib',
  '-T', 'psx/psx.ld'
]

def build_obj(source):
  return env.StaticObject('out/{}.o'.format(source), source)

def build_objs(dir):
  s_files = env.Glob('{}/*.s'.format(dir))
  c_files = env.Glob('{}/*.c'.format(dir))

  return [ build_obj(source) for source in (c_files + s_files) ]

crt = env.StaticLibrary('out/crt', build_objs('crt/src'))
psx = env.StaticLibrary('out/psx', build_objs('psx/src'))

tests = [dir for dir in listdir('src') if path.isdir(path.join('src', dir))]

for test in tests:
  test_elf = env.Program('out/{}'.format(test),
                         build_objs('src/{}'.format(test)),
                         LIBS = [ crt, psx ])

  test_psexe = 'out/{}.psexe'.format(test)

  env.Command(test_psexe, test_elf, toolchain_dir + 'elf2psexe NA $SOURCE $TARGET')
