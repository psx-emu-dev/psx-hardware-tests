.include "common.s"

.global main
.type main, function

main:
    push $s0, $s1, $s2, $s3, $ra, $fp

    println "Running test.."

    li $s0, 0xfffe0130
    sw $zero, 0x0($s0)
.rept 4
    sw $zero, 0x0($zero)
.endr

    la   $s0, test_words
    li   $s1, 0xfafafafa
    move $s2, $s1

    lw   $s1, 0x0($s0)
    lw   $s1, 0x4($s0)
    lw   $s1, 0x8($s0)
    lw   $s1, 0xc($s0)
    move $s3, $s1

    beq $s2, $s3, passed
    nop

    println "Test failed."
    j finish
    li $v0, 1

passed:
    println "Test passed."
    li $v0, 0

finish:
    pull $s0, $s1, $s2, $s3, $ra, $fp
    jr   $ra
    nop

.data

test_words:
.word 0x12345678
.word 0xdeadbeef
.word 0xdeadbead
.word 0xcafebabe
