This test verifies the behavior seen when reading unmapped memory regions from
a memory region with instruction cache enabled. The expected behavior is that
the last loaded instruction is returned instead, a concept known as "open bus".
Since we are in a cached region, this will always be an instruction at an
address ending in `c` because of how the caching works.

Specifically this test reads from `1f801130`, which would be a fourth timer's
counter value, if one existed.

The specific patterns from hardware are shown below.

| offset | data | lw_addr | pc | ic_addr |
|--------|------|---------|----|---------|
| `0` | `8d130000` | `80010110` | `80010118` | `8001011c` |
| `0` | `8d130000` | `80010114` | `8001011c` | `8001011c` |
| `0` | `02002825` | `80010118` | `80010120` | `8001012c` |
| `0` | `02002825` | `8001011c` | `80010124` | `8001012c` |
| - | - | - | - | - |
| `4` | `8d120000` | `80010114` | `8001011c` | `8001011c` |
| `4` | `0c004022` | `80010118` | `80010120` | `8001012c` |
| `4` | `0c004022` | `8001011c` | `80010124` | `8001012c` |
| `4` | `0c004022` | `80010120` | `80010128` | `8001012c` |
| - | - | - | - | - |
| `8` | `24840207` | `80010118` | `80010120` | `8001012c` |
| `8` | `24840207` | `8001011c` | `80010124` | `8001012c` |
| `8` | `24840207` | `80010120` | `80010128` | `8001012c` |
| `8` | `24840207` | `80010124` | `8001012c` | `8001012c` |
| - | - | - | - | - |
| `c` | `3c048001` | `8001011c` | `80010124` | `8001012c` |
| `c` | `3c048001` | `80010120` | `80010128` | `8001012c` |
| `c` | `3c048001` | `80010124` | `8001012c` | `8001012c` |
| `c` | `3c048001` | `80010128` | `80010130` | `8001013c` |

To explain this data a bit:
- `offset` &mdash; One of four possible address offsets to start the test at.
- `data` &mdash; The first column is the value loaded from the unmapped memory.
- `lw_addr` &mdash; Address of the `lw` instruction used to load the data.
- `pc` &mdash; Where the program counter actually was when the `lw` was executed.
- `ic_addr` &mdash; The address that corresponds to the loaded data.

With this, you can see that whenever the `pc` is a multiple of 16, the returned
value changes, this is because the CPU has  loaded the next 4 instructions into
a cache line.

In the last example, it appears as though the value did not change, this is a
false negative since the same instruction appeared 16 bytes apart.
