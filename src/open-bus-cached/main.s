.include "common.s"
.include "segment.s"

.macro assert cnt, num, exp, act
    beq \act,\exp, 1f
    nop
    move $a1, \exp
    move $a2, \act
    printfn "Test #\cnt, assert #\num failed. Expected: %08x, actual: %08x"
    b    failed
    nop
1:
.endm

.macro run_test cnt, exp0, exp1, exp2, exp3
    li $t6, 0x1f801130

.balign 16

1:  la $t7, 1b

.balign 16

.rept \cnt
    nop
.endr
    lw   $t0, 0x00($t6)
    lw   $t1, 0x00($t6)
    lw   $t2, 0x00($t6)
    lw   $t3, 0x00($t6)
    lw   $t4, 0x1c($t7)
    lw   $t5, 0x2c($t7)
    lw   $t6, 0x3c($t7)

    assert \cnt, 0, \exp0, $t0
    assert \cnt, 1, \exp1, $t1
    assert \cnt, 2, \exp2, $t2
    assert \cnt, 3, \exp3, $t3
.endm

.global main
.type main, function

main:
    push $ra, $fp

    println "Running test.."

    jal enter_kseg0 # sanity check to make sure we're really in kseg0
    nop

    run_test 0, $t4, $t4, $t5, $t5
    run_test 1, $t4, $t5, $t5, $t5
    run_test 2, $t5, $t5, $t5, $t5
    run_test 3, $t5, $t5, $t5, $t6

passed:
    println "Test passed."

    pull $ra, $fp
    jr   $ra
    li   $v0, 0

failed:
    printfn "Test failed."

    pull $ra, $fp
    jr   $ra
    li   $v0, 1
