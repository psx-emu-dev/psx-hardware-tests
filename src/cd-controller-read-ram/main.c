#include "psx.h"
#include "test.h"

/* Bank between CD registers */
static void cd_index(uint8_t index) {
  write8(CD_BASE + 0, index);
}

static void cd_irq_mask(uint8_t mask) {
  cd_index(1);

  write8(CD_BASE + 2, mask);
}

static void cd_irq_ack(void) {
  cd_index(1);

  /* Clear IRQ + clear host param FIFO */
  write8(CD_BASE + 3, 0x1f | 0x40);
}

static void cd_run_command(uint8_t *cmd, unsigned len) {
  unsigned i;
  uint8_t irq;
  uint8_t resp;

  cd_irq_ack();

  /* push parameters */
  cd_index(0);
  for (i = 1; i < len; i++) {
    write8(CD_BASE + 2, cmd[i]);
  }

  /* Start command */
  write8(CD_BASE + 1, cmd[0]);

  /* Wait for IRQ */
  cd_index(1);
  do {
    irq = read8(CD_BASE + 3) & 0x1f;
  } while (irq == 0);
}

int main() {
  uint8_t cmd[10] = {0};
  unsigned i;

  INFO("Dumping controller I/O + RAM");

  /* Mask interrupts */
  cd_irq_mask(0);

  for (i = 0; i < 0x300; i++) {
    uint8_t b;

    /* Send test command to read controller memory */
    cmd[0] = 0x19;
    cmd[1] = 0x60;
    cmd[2] = i;
    cmd[3] = i >> 8;

    cd_run_command(cmd, 4);

    /* Read response byte */
    b = read8(CD_BASE + 1);
    bios_printf("%04x: %02x\n", i, b);
  }

  INFO("All tests complete.");

  return 0;
}
