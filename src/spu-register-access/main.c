#include "psx.h"

int main(void) {
  /* Test using the "voice noise enable" register (24 bits) */
  const uint32_t test_reg = SPU_BASE + 0x194;
  unsigned i;
  unsigned failures = 0;

#define TEST(_a, _w, _e) do { \
  uint ## _w ## _t _r = read ## _w(_a); \
  if ((_e) != (_r)) { \
    bios_printf("Expected 0x%x read 0x%x\n", (_e), (_r)); \
    failures++; \
  } } while (0)

  // 32bit reads work as expected
  bios_printf("Word read at 0x%08x\n", test_reg);
  write16(test_reg, 0);
  write16(test_reg + 2, 0);
  TEST(test_reg, 32, 0);
  write16(test_reg, 0xef55);
  write16(test_reg + 2, 0xabcd);
  TEST(test_reg, 32, 0xabcdef55);

  // 16bit reads work as expected
  for (i = 0; i < 4; i += 2) {
    bios_printf("Halfword read at 0x%08x\n", test_reg + i);
    TEST(test_reg + i, 16, (0xabcdef55 >> (i * 8)) & 0xffff);
  }

  // 8bit reads work as expected
  for (i = 0; i < 4; i++) {
    bios_printf("Byte read at 0x%08x\n", test_reg + i);
    TEST(test_reg + i, 8, (0xabcdef55 >> (i * 8)) & 0xff);
  }

  // Word writes behave as expected, only they sometimes miss the bottom half of
  // the word (hence the doubled writes here to try and make sure they go
  // through)
  bios_printf("Word write at 0x%08x\n", test_reg + i);
  write32(test_reg, 0xabcdef55);
  write32(test_reg, 0xabcdef55);
  TEST(test_reg, 32, 0xabcdef55);
  write32(test_reg, 0x11223344);
  write32(test_reg, 0x11223344);
  TEST(test_reg, 32, 0x11223344);

  bios_printf("32bit write unstability: back-to-back 32bit writes miss "
              "the lower half of the 2nd write\n");
  // Attempt to showcase write 32 unreliability.
  for (i = 0; i < 30; i++) {
    uint32_t r;
    const uint32_t expected = 0xaabbccdd;

    write32(test_reg, 0x11223344);
    write32(test_reg, expected);
    r = read32(test_reg);
    if (r != expected) {
      bios_printf("Got one at try #%u: expected 0x%08x got 0x%08x\n",
                  i, expected, r);

      // Normally the top half should be ok, the bottom half should have the old
      // value
      if (r != 0xaabb3344) {
        bios_printf("Unpected pattern\n");
        failures++;
      }
    }
  }

  // Halfword writes behave as expected
  for (i = 0; i < 4; i += 2) {
    uint32_t expected = 0xabcdef55;

    expected &= ~(0xffff << (i * 8));
    expected |= (0x1230 + i) << (i * 8);

    bios_printf("Halfword write at 0x%08x\n", test_reg + i);

    write16(test_reg, 0xef55);
    write16(test_reg + 2, 0xabcd);
    write16(test_reg + i, 0x1230 + i);
    TEST(test_reg, 32, expected);
  }

  // Byte writes behave like halfword when halfword-aligned, do nothing
  // otherwise
  for (i = 0; i < 4; i++) {
    uint32_t expected = 0xabcdef55;

    if ((i & 1) == 0) {
      // We're halfword-aligne
      expected &= ~(0xffff << (i * 8));
      expected |= (0x00a0 + i) << (i * 8);
    }

    bios_printf("Byte write at 0x%08x\n", test_reg + i);
    write16(test_reg, 0xef55);
    write16(test_reg + 2, 0xabcd);
    write8(test_reg + i, 0xa0 + i);
    TEST(test_reg, 32, expected);
  }

  write16(test_reg, 0xef55);
  write16(test_reg + 2, 0xabcd);

  bios_printf("Byte write 16bit register value\n");
  // We use the 8 bit "SB" instruction but the full word is sent on the bus and
  // the SPU decides to use the full 16bits.
  __asm__ __volatile__ ("sb %0, 0(%1)" : : "r"(0x44332211), "r"(test_reg));
  TEST(test_reg, 32, 0xabcd2211);

  if (failures > 0) {
    bios_printf("%u test(s) failed\n", failures);
    return EXIT_FAILURE;
  } else {
    bios_printf("All tests passed\n");
    return EXIT_SUCCESS;
  }
}
