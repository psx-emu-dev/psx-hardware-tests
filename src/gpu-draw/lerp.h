#pragma once

#include "fp.h"

typedef struct {
  int v; // fixed-point
  int d; // fixed-point
} lerp_t;

lerp_t lerp_init(int a, int b, int steps);
