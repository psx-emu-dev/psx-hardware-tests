#pragma once

#include "gpu.h"

struct gpu_bounding_box_t {
  gpu_point_t tl;
  gpu_point_t br;
  gpu_size_t size;
};

extern void draw_pixel(uint16_t *fb, int x, int y, uint16_t color);

/**
 * Draws a line with the GPU.
 */
void line_test_gpu_draw(
    gpu_point_t a,
    gpu_color_t color_a,
    gpu_point_t b,
    gpu_color_t color_b,
    struct gpu_bounding_box_t bbox,
    uint16_t *fb);

/**
 * Draws a line with the simulation.
 */
void line_test_sim_draw(
    gpu_point_t a,
    gpu_color_t color_a,
    gpu_point_t b,
    gpu_color_t color_b,
    uint16_t *fb);
