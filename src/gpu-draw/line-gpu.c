#include "line.h"
#include "bios.h"

#define STEP_CURSOR()                 \
  do {                                \
    if (++x == bbox.size.width) {     \
      x = 0;                          \
      if (++y == bbox.size.height) {  \
        goto done;                    \
      }                               \
    }                                 \
  } while (0)

#define LOWER(x) ((uint16_t) (x))
#define UPPER(x) ((uint16_t) ((x) >> 16))

void line_test_gpu_draw(
    gpu_point_t a,
    gpu_color_t color_a,
    gpu_point_t b,
    gpu_color_t color_b,
    struct gpu_bounding_box_t bbox,
    uint16_t *fb) {
  gpu_gp0_50_draw_line(
      a, color_a,
      b, color_b);

  gpu_gp0_c0_copy(bbox.tl, bbox.size);

  int x = 0;
  int y = 0;

  while (1) {
    uint32_t data = gpu_read_data();

    draw_pixel(fb, bbox.tl.x + x, bbox.tl.y + y, LOWER(data));

    STEP_CURSOR();

    draw_pixel(fb, bbox.tl.x + x, bbox.tl.y + y, UPPER(data));

    STEP_CURSOR();
  }

  done: {
    gpu_color_t clear_color = { 0, 0, 0 };
    gpu_gp0_02_fill_area(bbox.tl, bbox.size, clear_color);
  }
}
