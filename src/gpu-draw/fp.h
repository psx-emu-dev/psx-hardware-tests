#pragma once

#define FP_BITS 12
#define FP_OF_INT(x) ((x) << FP_BITS)
#define FP_TO_INT(x) ((x) >> FP_BITS)
#define FP_ONE_HALF (FP_OF_INT(1) / 2)
