#include "line.h"
#include "bios.h"
#include "lerp.h"

int abs(int x) {
  return x < 0 ? -x : x;
}

int max(int a, int b) {
  return a > b ? a : b;
}

static uint16_t rgb_24_16(int r, int g, int b) {
  return
      ((r >> 3) <<  0) |
      ((g >> 3) <<  5) |
      ((b >> 3) << 10);
}

static void line_test_sim_draw_sorted(
    gpu_point_t point_a, gpu_color_t color_a,
    gpu_point_t point_b, gpu_color_t color_b,
    uint16_t *fb) {
  int dx = abs(point_b.x - point_a.x);
  int dy = abs(point_b.y - point_a.y);
  int ns = max(dx, dy);
  dy = -dy;

  int er = dx + dy;
  int cx = point_a.x;
  int cy = point_a.y;
  int sx = point_a.x < point_b.x ? 1 : -1;
  int sy = point_a.y < point_b.y ? 1 : -1;

  lerp_t r = lerp_init(color_a.r, color_b.r, ns);
  lerp_t g = lerp_init(color_a.g, color_b.g, ns);
  lerp_t b = lerp_init(color_a.b, color_b.b, ns);

  for (int i = 0; i <= ns; i++) {
    draw_pixel(fb, cx, cy, rgb_24_16(
      FP_TO_INT(r.v),
      FP_TO_INT(g.v),
      FP_TO_INT(b.v)));

    r.v += r.d;
    g.v += g.d;
    b.v += b.d;

    int e2 = 2 * er;

    if (e2 > dy) {
      er += dy;
      cx += sx;
    }

    if (e2 <= dx) {
      er += dx;
      cy += sy;
    }
  }
}

void line_test_sim_draw(
    gpu_point_t point_a, gpu_color_t color_a,
    gpu_point_t point_b, gpu_color_t color_b,
    uint16_t *fb) {

  if (point_a.x == point_b.x && point_a.y == point_b.y) {
    return line_test_sim_draw_sorted(
        point_a, color_a,
        point_b, color_b,
        fb);
  }

  if (point_a.x >= point_b.x) {
    return line_test_sim_draw_sorted(
        point_b, color_b,
        point_a, color_a,
        fb);
  }

  return line_test_sim_draw_sorted(
      point_a, color_a,
      point_b, color_b,
      fb);
}
