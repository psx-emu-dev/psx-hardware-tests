#include "lerp.h"
#include "bios.h"

lerp_t lerp_init(int a, int b, int steps) {
  lerp_t result;

  result.v = FP_OF_INT(a) + FP_ONE_HALF;
  result.d = 0;

  if (steps) {
    result.d = FP_OF_INT(b - a) / steps;
  }

  return result;
}
