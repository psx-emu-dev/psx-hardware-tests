#include "line.h"
#include "bios.h"

#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define MAX(x, y) (((x) > (y)) ? (x) : (y))

static void bbox_init(
    gpu_point_t a,
    gpu_point_t b,
    struct gpu_bounding_box_t *bbox) {

  gpu_point_t tl;
  tl.x = MIN(a.x, b.x);
  tl.y = MIN(a.y, b.y);

  gpu_point_t br;
  br.x = MAX(a.x, b.x);
  br.y = MAX(a.y, b.y);

  gpu_size_t size;
  size.width = (br.x + 1) - tl.x;
  size.height = (br.y + 1) - tl.y;

  bbox->tl = tl;
  bbox->br = br;
  bbox->size = size;
}

#undef MIN
#undef MAX

const int size = 64;

bool fb_cmp(uint16_t *gpu_fb, uint16_t *sim_fb, struct gpu_bounding_box_t bbox) {
  // bias pointers to start at the top-left of the bounding box
  gpu_fb += (bbox.tl.y * size) + bbox.tl.x;
  sim_fb += (bbox.tl.y * size) + bbox.tl.x;

  for (int y = 0; y < bbox.size.height; y++) {
    for (int x = 0; x < bbox.size.width; x++) {
      uint16_t gpu = *(gpu_fb + x);
      uint16_t sim = *(sim_fb + x);

      if (gpu != sim) {
        return false;
      }
    }

    gpu_fb += size;
    sim_fb += size;
  }

  return true;
}

void fb_dump(uint16_t *gpu_fb) {
  for (int y = 0; y < size; y++) {
    for (int x = 0; x < size; x++) {
      bios_printf("%04x, ", *gpu_fb);
      gpu_fb++;
    }

    bios_printf("\n");
  }
}

void draw_pixel(uint16_t *fb, int x, int y, uint16_t color) {
  int offset = (y * size) + x;
  *(fb + offset) = color;
}

int main() {
  void *heap_addr = (void *)(0x80020000);
  size_t heap_size = 65536;
  bios_init_heap(heap_addr, heap_size);

  gpu_point_t drawing_area_tl = { 0, 0 };
  gpu_point_t drawing_area_br = { 0x3ff, 0x1ff };

  gpu_gp1_00_reset();
  gpu_gp0_set_drawing_area_tl(drawing_area_tl);
  gpu_gp0_set_drawing_area_br(drawing_area_br);

  gpu_point_t a = { size / 2, size / 2 };
  gpu_point_t b = { 0, 0 };

  int fb_size = size * size * 2;

  uint16_t *gpu_fb = bios_malloc(fb_size);
  uint16_t *sim_fb = bios_malloc(fb_size);

  gpu_color_t color_a = { 255, 0, 0 };
  gpu_color_t color_b = { 0, 0, 255 };

  for (b.y = 0; b.y < size; b.y++) {
    bios_printf("Test batch %d/%d (%d cases)\n", b.y + 1, size, size);

    for (b.x = 0; b.x < size; b.x++) {
      struct gpu_bounding_box_t bbox;
      bbox_init(a, b, &bbox);

      bios_memset(gpu_fb, 0, fb_size);
      bios_memset(sim_fb, 0, fb_size);

      line_test_gpu_draw(
          a, color_a,
          b, color_b,
          bbox, gpu_fb);

      line_test_sim_draw(
          a, color_a,
          b, color_b, sim_fb);

      bool result = fb_cmp(gpu_fb, sim_fb, bbox);

      bios_free(sim_fb);
      bios_free(gpu_fb);

      if (!result) {
        bios_printf("Test failed. [(%d, %d) -> (%d, %d)]\n",
          a.x, a.y,
          b.x, b.y);

        bios_printf("GPU buffer:\n");
        fb_dump(gpu_fb);

        bios_printf("SIM buffer:\n");
        fb_dump(sim_fb);
        return 1;
      }
    }
  }

  bios_printf("All tests passed.\n");
  return 0;
}
