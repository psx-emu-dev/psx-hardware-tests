#include "psx.h"
#include "test.h"

static void assert_quo(int expected, int actual) {
  ASSERT_EQ(expected, actual, "Quotient is wrong. Expected '%08x', got '%08x'");
}

static void assert_rem(int expected, int actual) {
  ASSERT_EQ(expected, actual, "Remainder is wrong. Expected '%08x', got '%08x'");
}

static void signed_division(int num, int div, int expected_quo, int expected_rem) {
  INFO("Running test case for signed division: %08x / %08x", num, div);

  int quo;
  int rem;

  asm volatile(
    "div $zero, %2, %3\n"
    "mflo %0\n"
    "mfhi %1\n"
      : "=r" (quo), "=r" (rem)
      :  "r" (num),  "r" (div)
  );

  assert_quo(expected_quo, quo);
  assert_rem(expected_rem, rem);
}

static void unsigned_division(int num, int div, int expected_quo, int expected_rem) {
  INFO("Running test case for unsigned division: %08x / %08x", num, div);

  int quo;
  int rem;

  asm volatile(
    "divu $zero, %2, %3\n"
    "mflo %0\n"
    "mfhi %1\n"
      : "=r" (quo), "=r" (rem)
      :  "r" (num),  "r" (div)
  );

  assert_quo(expected_quo, quo);
  assert_rem(expected_rem, rem);
}

int main() {
  unsigned_division(0x7fffffff, 0x00000000, 0xffffffff, 0x7fffffff);

  signed_division(0x7fffffff, 0x00000000, 0xffffffff, 0x7fffffff);
  signed_division(0xffffffff, 0x00000000, 0x00000001, 0xffffffff);
  signed_division(0x80000000, 0xffffffff, 0x80000000, 0x00000000);
  signed_division(0xffffffff, 0x80000000, 0x00000000, 0xffffffff);
  signed_division(0x80000001, 0x80000000, 0x00000000, 0x80000001);

  INFO("All tests complete.");
  return 0;
}
