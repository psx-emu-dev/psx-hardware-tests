/* Dual Shock protocol test
 *
 * Test conditions: one Dual Shock controller (SCPH-1200) plugged on Port 1 with
 * no button pressed and sticks at the center. Initial analog state doesn't
 * matter since we'll reconfigure it ourselves.
 *
 * Things that can't be tested automatically:
 *
 * - I haven't found a way to get the analog lock state programmatically:
 *
 *   > Only the 2 LSBs of the lock/unlock byte appear to matter, so 0x3 and 0xff
 *     both lock, 0x2 and 0xa both unlock.
 *
 *   > Lock is applied even if the previous byte (analog enable/disable) is
 *     invalid.
 *
 * - The red LED is lit when analog mode is true. Since analog input can always
 *   be read when in Dual Shock mode it's possible for the analog sticks to be
 *   usable with the analog LED off.
 *
 * - Rumble shenanigans:
 *
 *   > Once unlocked with command 4D the motors are controlled by setting bytes
 *     3 and 4 of 0x42. This works even if you return to normal mode once
 *     unlocked.
 *
 *   > Attempt to drive the rumble motors while they're locked do nothing. Once
 *     unlocked the motors start powered off regardless of previously written
 *     values.
 *
 *   > The small motor is controlled by the LSB of byte 3 by default. It's on or
 *     off, no in-between. Other bits are ignored.
 *
 *   > The big motor is controlled by byte 4 by default and can run at various
 *     speeds. The bigger the number the faster it rotates, maxing out at 0xff.
 *
 *   > On my controller the big motor manages to start with value 0x41. Below
 *     that it doesn't manage to complete a rotation.
 *
 *   > Once started on my controller the motor keeps rotating correctly with
 *     values as low as 0x21. Below that the motor loses momentums and stops
 *     after a couple of rotations. Obviously these specific values will depend
 *     on a number of factors including the position of the motor.
 *
 *   > The values above are with the motor almost horizontal (worst case
 *     scenario since the motor has to move the counterweight against gravity).
 *     With the motor hanging vertically I can get the motor to start and run
 *     with values as low as 0x13 and keep rotating (very slowly) at 0x11.
 */

#include "psx.h"
#include "test.h"

#define PORT_1  (0U << 13)
#define PORT_2  (1U << 13)

static void controller_init(void) {
    /* The standard baud rate for PSX devices */
    const uint16_t baud_rate = 0x88;

    /* Reset the controller */
    write16(JOY_MC_BASE + 0xa, 0x40);

    write16(JOY_MC_BASE + 0xa, 0x0);

    /* Set baud rate */
    write16(JOY_MC_BASE + 0xe, baud_rate);

    /* Set mode: 8bits no parity MUL1 */
    write16(JOY_MC_BASE + 0x8, 0xd);
}

/* Communicate with the device: we send the `length` bytes in `to_send`, waiting
 * for the DSR from controller every time (except for the last bit). If
 * `response` is not NULL we store the response bytes in it. If the device does
 * not assert the DSR after a reasonable period of time the transfer is aborted.
 *
 * if `response` is not NULL it should be at least `length` bytes long.
 *
 * The return value is the number of bytes successfully exchanged (including the
 * last exchange that didn't set the DSR).
 */
static unsigned controller_send(uint16_t port,
                                unsigned length,
                                const uint8_t *to_send,
                                uint8_t *response) {
    unsigned i;

    /* TX enable, output selection down on the right port */
    write16(JOY_MC_BASE + 0xa, 0x3 | port);
    busy_loop(1000);

    for (i = 0; i < length; i++) {
        uint8_t r;

        /* If we're not at the first byte, we wait for DSR */
        if (i > 0) {
            /* As far as I know the longest possible time for the DSR to happen
             * is during byte 6 of a memory card read: it takes more than 30
             * thousand cycles to happen. For other exchanges it's usually a few
             * hundred cycles at most. */
            unsigned timeout = 100 * 1000;
            uint32_t stat = 0;

            while (timeout--) {
                stat = read32(JOY_MC_BASE + 0x4);
                if (stat & (1U << 7)) {
                    /* We have DSR */
                    break;
                }
            }

            if (!(stat & (1U << 7))) {
                /* No DSR pulse, abort. */
                break;
            }
        }

        /* Send byte */
        write8(JOY_MC_BASE + 0x0, to_send[i]);
        /* Wait for RX not empty, no timeout here because unless the hardware is
         * dysfunctional there shouldn't be any way that it ever occurs */
        while ((read32(JOY_MC_BASE + 0x4) & (1U << 1)) == 0) {
            ;
        }

        /* Read response */
        r = read8(JOY_MC_BASE + 0x0);
        if (response) {
            response[i] = r;
        }
    }

    /* De-select and unset TX enable */
    write16(JOY_MC_BASE + 0xa, 0);
    busy_loop(1000);

    return i;
}

int main(void) {
    uint8_t msg[10] = {0};
    uint8_t resp[10] = {0};
    unsigned rx_len;
    unsigned test_failed = 0;

#define CHECK(_pos, _expected) do { \
    if (_pos < rx_len && resp[_pos] != _expected) { \
        ERROR("Byte %u: expected 0x%02x got 0x%02x", \
                    _pos, _expected, resp[_pos]); \
        test_failed++; \
    }} while (0)

#define CHECK_NOT(_pos, _unexpected) do { \
    if (_pos < rx_len && resp[_pos] == _unexpected) { \
        ERROR("Byte %u has invalid value 0x%02x", \
                    _pos, _unexpected); \
        test_failed++; \
    }} while (0)

#define CHECK_LEN(_expected) do { \
    if (rx_len != _expected) { \
        ERROR("Command should return %u bytes, got %u", \
                    _expected, rx_len); \
        test_failed++; \
    } } while (0)

    controller_init();

    INFO("Switch controller to Dual Shock mode (43 00 01)");

    msg[0] = 0x01; /* Select controller */
    msg[1] = 0x43;
    msg[2] = 0x00;
    msg[3] = 0x01; /* 0x01: enter Dual Shock mode */
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    /* I always attempt to send more bytes than needed to see if the controller
     * sends spurious DSR pulses after the normal end of the transaction */
    rx_len = controller_send(PORT_1, 10, msg, resp);

    /* We receive 5 bytes if we were in digital mode, 9 in analog */
    if (rx_len != 5 && rx_len != 9) {
        ERROR("Couldn't switch controller to Dual Shock mode! [%x]", rx_len);
        return EXIT_FAILURE;
    }

    INFO("Disable analog, unlocked");

    msg[0] = 0x01; /* Select controller */
    msg[1] = 0x44; /* 0x44: set analog mode + lock */
    msg[2] = 0x00;
    msg[3] = 0x00; /* 0x00: disable analog mode */
    msg[4] = 0x02; /* 0x02: unlocked */
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0);
    CHECK(4, 0);
    CHECK(5, 0);
    CHECK(6, 0);
    CHECK(7, 0);
    CHECK(8, 0);

    INFO("Read analog state");

    msg[0] = 0x01;
    msg[1] = 0x45;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x01);
    CHECK(4, 0x02);
    CHECK(5, 0x00); /* Analog state */
    CHECK(6, 0x02);
    CHECK(7, 0x01);
    CHECK(8, 0x00);

    INFO("Read buttons (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    /* We're in Dual Shock mode, even though we disabled the analog mode we
     * still receive the full input state including the sticks */
    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    /* Assume no buttons are pressed for the sake of the test */
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    /* The 4 analog stick positions follow. We can't really test that because
     * even when left at the center the values drift significantly from the
     * expected 0x80 center value. Still, if the sticks at are rest and the
     * controller is functional the values shouldn't be maxed out, so let's
     * validate that
     */
    CHECK_NOT(5, 0xff);
    CHECK_NOT(5, 0);
    CHECK_NOT(6, 0xff);
    CHECK_NOT(6, 0);
    CHECK_NOT(7, 0xff);
    CHECK_NOT(7, 0);
    CHECK_NOT(8, 0xff);
    CHECK_NOT(8, 0);

    INFO("Switch controller to Normal mode (43 00 00)");

    msg[0] = 0x01;
    msg[1] = 0x43;
    msg[2] = 0x00;
    msg[3] = 0x00; /* 0x00: normal mode */
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x00);
    CHECK(6, 0x00);
    CHECK(7, 0x00);
    CHECK(8, 0x00);

    INFO("Read buttons (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    /* We've deactivated the analog mode above and we've returned to normal
     * mode, we should only be reading digital values */
    CHECK_LEN(5);
    CHECK(0, 0xff);
    CHECK(1, 0x41);
    CHECK(2, 0x5a);
    /* Assume no buttons are pressed for the sake of the test */
    CHECK(3, 0xff);
    CHECK(4, 0xff);

    INFO("Switch controller to Dual Shock mode (43 00 01)");

    msg[0] = 0x01; /* Select controller */
    msg[1] = 0x43;
    msg[2] = 0x00;
    msg[3] = 0x01; /* 0x01: enter Dual Shock mode */
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    /* I always attempt to send more bytes than needed to see if the controller
     * sends spurious DSR pulses after the normal end of the transaction */
    rx_len = controller_send(PORT_1, 10, msg, resp);

    /* We know analog is disabled so we should get the same response as with 42
     */
    CHECK_LEN(5);
    CHECK(0, 0xff);
    CHECK(1, 0x41);
    CHECK(2, 0x5a);
    /* Assume no buttons are pressed for the sake of the test */
    CHECK(3, 0xff);
    CHECK(4, 0xff);

    INFO("Enable analog, unlocked");

    msg[0] = 0x01; /* Select controller */
    msg[1] = 0x44; /* 0x44: set analog mode + lock */
    msg[2] = 0x00;
    msg[3] = 0x01; /* 0x01: disable analog mode */
    msg[4] = 0x02; /* 0x02: unlocked */
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0);
    CHECK(4, 0);
    CHECK(5, 0);
    CHECK(6, 0);
    CHECK(7, 0);
    CHECK(8, 0);

    INFO("Read analog state");

    msg[0] = 0x01;
    msg[1] = 0x45;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x01);
    CHECK(4, 0x02);
    CHECK(5, 0x01); /* Analog state */
    CHECK(6, 0x02);
    CHECK(7, 0x01);
    CHECK(8, 0x00);

    INFO("Read buttons (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    /* We're in Dual Shock mode, even though we disabled the analog mode we
     * still receive the full input state including the sticks */
    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    /* Assume no buttons are pressed for the sake of the test */
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    /* The 4 analog stick positions follow. We can't really test that because
     * even when left at the center the values drift significantly from the
     * expected 0x80 center value. Still, if the sticks at are rest and the
     * controller is functional the values shouldn't be maxed out, so let's
     * validate that
     */
    CHECK_NOT(5, 0xff);
    CHECK_NOT(5, 0);
    CHECK_NOT(6, 0xff);
    CHECK_NOT(6, 0);
    CHECK_NOT(7, 0xff);
    CHECK_NOT(7, 0);
    CHECK_NOT(8, 0xff);
    CHECK_NOT(8, 0);

    INFO("Switch controller to Normal mode (43 00 00)");

    msg[0] = 0x01;
    msg[1] = 0x43;
    msg[2] = 0x00;
    msg[3] = 0x00; /* 0x00: normal mode */
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x00);
    CHECK(6, 0x00);
    CHECK(7, 0x00);
    CHECK(8, 0x00);

    INFO("Read buttons (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    /* We've enable the analog mode above and we've returned to normal
     * mode, we should still be in analog mode */
    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0x73);
    CHECK(2, 0x5a);
    /* Assume no buttons are pressed for the sake of the test */
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    /* The 4 analog stick positions follow. We can't really test that because
     * even when left at the center the values drift significantly from the
     * expected 0x80 center value. Still, if the sticks at are rest and the
     * controller is functional the values shouldn't be maxed out, so let's
     * validate that
     */
    CHECK_NOT(5, 0xff);
    CHECK_NOT(5, 0);
    CHECK_NOT(6, 0xff);
    CHECK_NOT(6, 0);
    CHECK_NOT(7, 0xff);
    CHECK_NOT(7, 0);
    CHECK_NOT(8, 0xff);
    CHECK_NOT(8, 0);

    INFO("Switch controller to Dual Shock mode (43 00 01)");

    msg[0] = 0x01; /* Select controller */
    msg[1] = 0x43;
    msg[2] = 0x00;
    msg[3] = 0x01; /* 0x01: enter Dual Shock mode */
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    /* We know analog is enabled so we should get the same response as with 42
     */
    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0x73);
    CHECK(2, 0x5a);
    /* Assume no buttons are pressed for the sake of the test */
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    /* The 4 analog stick positions follow. We can't really test that because
     * even when left at the center the values drift significantly from the
     * expected 0x80 center value. Still, if the sticks at are rest and the
     * controller is functional the values shouldn't be maxed out, so let's
     * validate that
     */
    CHECK_NOT(5, 0xff);
    CHECK_NOT(5, 0);
    CHECK_NOT(6, 0xff);
    CHECK_NOT(6, 0);
    CHECK_NOT(7, 0xff);
    CHECK_NOT(7, 0);
    CHECK_NOT(8, 0xff);
    CHECK_NOT(8, 0);

    INFO("Disable analog, unlocked");

    msg[0] = 0x01; /* Select controller */
    msg[1] = 0x44; /* 0x44: set analog mode + lock */
    msg[2] = 0x00;
    msg[3] = 0x00; /* 0x00: disable analog mode */
    msg[4] = 0x02; /* 0x02: unlocked */
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0);
    CHECK(4, 0);
    CHECK(5, 0);
    CHECK(6, 0);
    CHECK(7, 0);
    CHECK(8, 0);

    INFO("Read analog state");

    msg[0] = 0x01;
    msg[1] = 0x45;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x01);
    CHECK(4, 0x02);
    CHECK(5, 0x00); /* Analog state */
    CHECK(6, 0x02);
    CHECK(7, 0x01);
    CHECK(8, 0x00);

    INFO("Bad analog, unlocked");

    msg[0] = 0x01; /* Select controller */
    msg[1] = 0x44; /* 0x44: set analog mode + lock */
    msg[2] = 0x00;
    /* All values >= 2 behave the same */
    msg[3] = 0x02; /* 0x02: invalid */
    /* Important note (since I don't know how to test for this yet): the
     * lock/unlock is applied even if the analog byte above is invalid */
    msg[4] = 0x02; /* 0x02: unlocked */
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0);
    CHECK(4, 0);
    CHECK(5, 0xff); /* This seems to notify the error */
    CHECK(6, 0);
    CHECK(7, 0);
    CHECK(8, 0);

    /* Analog state shouldn't change when we issue a bad command */
    INFO("Read analog state");

    msg[0] = 0x01;
    msg[1] = 0x45;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x01);
    CHECK(4, 0x02);
    CHECK(5, 0x00); /* Analog state */
    CHECK(6, 0x02);
    CHECK(7, 0x01);
    CHECK(8, 0x00);

    INFO("Bad lock");

    msg[0] = 0x01; /* Select controller */
    msg[1] = 0x44; /* 0x44: set analog mode + lock */
    msg[2] = 0x00;
    msg[3] = 0x01; /* 0x01: analog enabled */
    /* Important note: it seems that only the 2 LBSs are tested */
    msg[4] = 0x00; /* 0x00: invalid */
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0);
    CHECK(4, 0);
    CHECK(5, 0); /* Error is not reported here */
    CHECK(6, 0);
    CHECK(7, 0);
    CHECK(8, 0);

    /* Analog state should change even if the lock is invalid */
    INFO("Read analog state");

    msg[0] = 0x01;
    msg[1] = 0x45;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x01);
    CHECK(4, 0x02);
    CHECK(5, 0x01); /* Analog state */
    CHECK(6, 0x02);
    CHECK(7, 0x01);
    CHECK(8, 0x00);

    INFO("Dummy command 40");

    msg[0] = 0x01;
    msg[1] = 0x40;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x00);
    CHECK(6, 0x00);
    CHECK(7, 0x00);
    CHECK(8, 0x00);

    INFO("Dummy command 41");

    msg[0] = 0x01;
    msg[1] = 0x41;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x00);
    CHECK(6, 0x00);
    CHECK(7, 0x00);
    CHECK(8, 0x00);

    INFO("Dummy command 49");

    msg[0] = 0x01;
    msg[1] = 0x49;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x00);
    CHECK(6, 0x00);
    CHECK(7, 0x00);
    CHECK(8, 0x00);

    INFO("Dummy command 4a");

    msg[0] = 0x01;
    msg[1] = 0x4a;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x00);
    CHECK(6, 0x00);
    CHECK(7, 0x00);
    CHECK(8, 0x00);

    INFO("Dummy command 4b");

    msg[0] = 0x01;
    msg[1] = 0x4b;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x00);
    CHECK(6, 0x00);
    CHECK(7, 0x00);
    CHECK(8, 0x00);

    INFO("Dummy command 4e");

    msg[0] = 0x01;
    msg[1] = 0x4e;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x00);
    CHECK(6, 0x00);
    CHECK(7, 0x00);
    CHECK(8, 0x00);

    INFO("Dummy command 4f");

    msg[0] = 0x01;
    msg[1] = 0x4f;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x00);
    CHECK(6, 0x00);
    CHECK(7, 0x00);
    CHECK(8, 0x00);

    INFO("Mystery command 46 00 00");

    msg[0] = 0x01;
    msg[1] = 0x46;
    msg[2] = 0x00;
    msg[3] = 0x00; /* Mystery option 00 */
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x01);
    CHECK(6, 0x02);
    CHECK(7, 0x00);
    CHECK(8, 0x0a);

    INFO("Mystery command 46 00 01");

    msg[0] = 0x01;
    msg[1] = 0x46;
    msg[2] = 0x00;
    msg[3] = 0x01; /* Mystery option 01 */
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x01);
    CHECK(6, 0x01);
    CHECK(7, 0x01);
    CHECK(8, 0x14);

    INFO("Mystery command 46 00 xx");

    msg[0] = 0x01;
    msg[1] = 0x46;
    msg[2] = 0x00;
    /* Any value different from 0 or 1 returns the same values. You can verify
     * by looping through all possible values of that byte from 2 to 0xff but
     * that takes a while so I prefer to only test one value. It's not like we
     * know what this does anyway */
    msg[3] = 0xab;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x00);
    CHECK(6, 0x00);
    CHECK(7, 0x00);
    CHECK(8, 0x00);

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0);
    CHECK(4, 0);
    CHECK(5, 0); /* Error is not reported here */
    CHECK(6, 0);
    CHECK(7, 0);
    CHECK(8, 0);

    INFO("Mystery command 47");

    msg[0] = 0x01;
    msg[1] = 0x47;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x02);
    CHECK(6, 0x00);
    CHECK(7, 0x01);
    CHECK(8, 0x00);

    INFO("Mystery command 48");

    msg[0] = 0x01;
    msg[1] = 0x48;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x00);
    CHECK(6, 0x00);
    CHECK(7, 0x01);
    CHECK(8, 0x00);

    INFO("Mystery command 4c 00 00");

    msg[0] = 0x01;
    msg[1] = 0x4c;
    msg[2] = 0x00;
    msg[3] = 0x00; /* Mystery option */
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x00);
    CHECK(6, 0x04);
    CHECK(7, 0x00);
    CHECK(8, 0x00);

    INFO("Mystery command 4c 00 01");

    msg[0] = 0x01;
    msg[1] = 0x4c;
    msg[2] = 0x00;
    msg[3] = 0x01; /* Mystery option */
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x00);
    CHECK(6, 0x07);
    CHECK(7, 0x00);
    CHECK(8, 0x00);

    INFO("Mystery command 4c 00 xx");

    msg[0] = 0x01;
    msg[1] = 0x4c;
    msg[2] = 0x00;
    msg[3] = 0xab; /* Mystery option. All values >= 2 are treated the same */
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x00);
    CHECK(6, 0x00);
    CHECK(7, 0x00);
    CHECK(8, 0x00);

    INFO("Unlock Rumble (4d)");

    msg[0] = 0x01;
    msg[1] = 0x4d;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x01;
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    /* This command returns the previously written values, since we don't know
     * them at this point we can't test them. */

    INFO("Unlock Rumble (4d)");

    msg[0] = 0x01;
    msg[1] = 0x4d;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x01;
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    /* We should read back the previous values */
    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x01);
    CHECK(5, 0xff);
    CHECK(6, 0xff);
    CHECK(7, 0xff);
    CHECK(8, 0xff);

    INFO("We should remain in DS mode if we poll the controller regularly");

    write16(JOY_MC_BASE + 0xa, 0x3 | PORT_1);
    for (unsigned i = 0; i < 10; i++) {
        /* Not long enough to trigger the timeout, but 10x should */
        busy_loop(4 * 1000 * 1000);

        /* We don't even need to transfer anything, just asserting the Selection
         * line from time to time is enough. Note that merely maintaining the
         * selection active all the time is not enough, it must reset on edge or
         * something. */
        rx_len = controller_send(PORT_1, 0, msg, resp);
        CHECK_LEN(0);
    }

    INFO("Read buttons (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    /* We should still be in DS mode */
    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    CHECK_NOT(5, 0xff);
    CHECK_NOT(5, 0);
    CHECK_NOT(6, 0xff);
    CHECK_NOT(6, 0);
    CHECK_NOT(7, 0xff);
    CHECK_NOT(7, 0);
    CHECK_NOT(8, 0xff);
    CHECK_NOT(8, 0);

    INFO("We should return to digital mode after ~2.5 seconds of inactivity.");

    /* After this we should be in digital mode with rumble switched off */
    busy_loop(40 * 1000 * 1000);

    INFO("Read buttons (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(5);
    CHECK(0, 0xff);
    CHECK(1, 0x41);
    CHECK(2, 0x5a);
    /* Assume no buttons are pressed for the sake of the test */
    CHECK(3, 0xff);
    CHECK(4, 0xff);

    INFO("Switch controller to Dual Shock mode (43 00 01)");

    msg[0] = 0x01; /* Select controller */
    msg[1] = 0x43;
    msg[2] = 0x00;
    msg[3] = 0x01; /* 0x01: enter Dual Shock mode */
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    /* I always attempt to send more bytes than needed to see if the controller
     * sends spurious DSR pulses after the normal end of the transaction */
    rx_len = controller_send(PORT_1, 10, msg, resp);

    /* We know analog is disabled so we should get the same response as with 42
     */
    CHECK_LEN(5);
    CHECK(0, 0xff);
    CHECK(1, 0x41);
    CHECK(2, 0x5a);
    /* Assume no buttons are pressed for the sake of the test */
    CHECK(3, 0xff);
    CHECK(4, 0xff);

    INFO("Enable analog, unlocked");

    msg[0] = 0x01; /* Select controller */
    msg[1] = 0x44; /* 0x44: set analog mode + lock */
    msg[2] = 0x00;
    msg[3] = 0x01; /* 0x01: disable analog mode */
    msg[4] = 0x02; /* 0x02: unlocked */
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0);
    CHECK(4, 0);
    CHECK(5, 0);
    CHECK(6, 0);
    CHECK(7, 0);
    CHECK(8, 0);

    INFO("Unlock Rumble (4d)");

    msg[0] = 0x01;
    msg[1] = 0x4d;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x01;
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    /* After watchdog reset values should return to full 0xff */
    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    CHECK(5, 0xff);
    CHECK(6, 0xff);
    CHECK(7, 0xff);
    CHECK(8, 0xff);

    INFO("Switch controller to Normal mode (43 00 00)");

    msg[0] = 0x01;
    msg[1] = 0x43;
    msg[2] = 0x00;
    msg[3] = 0x00; /* 0x00: normal mode */
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0x00);
    CHECK(6, 0x00);
    CHECK(7, 0x00);
    CHECK(8, 0x00);

    /* In Normal mode Dual Shock commands should be ignored (even if analog is
     * on) */
    for (unsigned cmd = 0x40; cmd < 0x50; cmd++) {
        if (cmd == 0x42 || cmd == 0x43) {
            /* Those work in normal mode */
            continue;
        }

        INFO("Normal mode invalid command %02x", cmd);

        msg[0] = 0x01;
        msg[1] = cmd;
        msg[2] = 0x00;

        rx_len = controller_send(PORT_1, 10, msg, resp);
        CHECK_LEN(2);
        CHECK(0, 0xff);
        CHECK(1, 0x73);
    }

    INFO("Read buttons (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    /* We should still be in analog mode */
    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0x73);
    CHECK(2, 0x5a);
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    CHECK_NOT(5, 0xff);
    CHECK_NOT(5, 0);
    CHECK_NOT(6, 0xff);
    CHECK_NOT(6, 0);
    CHECK_NOT(7, 0xff);
    CHECK_NOT(7, 0);
    CHECK_NOT(8, 0xff);
    CHECK_NOT(8, 0);

    INFO("We should return to digital mode after ~2.5 seconds of inactivity.");

    /* After this we should be in digital mode with rumble switched off */
    busy_loop(40 * 1000 * 1000);

    INFO("Read buttons (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(5);
    CHECK(0, 0xff);
    CHECK(1, 0x41);
    CHECK(2, 0x5a);
    /* Assume no buttons are pressed for the sake of the test */
    CHECK(3, 0xff);
    CHECK(4, 0xff);

    if (test_failed) {
        bios_printf("%u test(s) failed\n", test_failed);
        return EXIT_FAILURE;
    } else {
        bios_printf("All tests passed\n");
        return EXIT_SUCCESS;
    }
}
