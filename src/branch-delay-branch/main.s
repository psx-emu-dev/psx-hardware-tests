.include "common.s"

.global main
.type main, function

main:
    push $ra, $fp, $s0, $s1

    println "Running test.."

    jal target
    jr  $ra
    nop

    li  $s1, 1
    beq $s0, $s1, passed
    nop

    println "Test failed."
    j  finished
    li $v0, 1

passed:
    println "Test passed."
    li $v0, 0

finished:
    pull $ra, $fp, $s0, $s1
    jr   $ra
    nop

target:
    li $s0, 1
