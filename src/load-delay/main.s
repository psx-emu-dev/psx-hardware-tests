.include "common.s"

.macro load_delay_test op
    println "Testing load delays for \op .."
    li   $t0, 0xdeadbeef
    move $t1, $t0
    li   $t2, 0x0
    \op  $t1, 0x0($zero)
    move $t2, $t1 # value should be the same as $t0
    bne  $t2, $t0, failed
    nop
.endm

.global main
.type main, function

main:
    push $ra, $fp

    println "Running test.."

    load_delay_test lw
    load_delay_test lwl
    load_delay_test lwr
    load_delay_test lh
    load_delay_test lhu
    load_delay_test lb
    load_delay_test lbu

passed:
    println "Test passed."

    pull $ra, $fp
    jr   $ra
    li   $v0, 0

failed:
    println "Test failed."

    pull $ra, $fp
    jr   $ra
    li   $v0, 1
