#include "psx.h"
#include "test.h"

#define PORT_1  (0U << 13)
#define PORT_2  (1U << 13)

static void controller_init(void) {
    /* The standard baud rate for PSX devices */
    const uint16_t baud_rate = 0x88;

    /* Reset the controller */
    write16(JOY_MC_BASE + 0xa, 0x40);

    write16(JOY_MC_BASE + 0xa, 0x0);

    /* Set baud rate */
    write16(JOY_MC_BASE + 0xe, baud_rate);

    /* Set mode: 8bits no parity MUL1 */
    write16(JOY_MC_BASE + 0x8, 0xd);
}

/* Communicate with the device: we send the `length` bytes in `to_send`, waiting
 * for the DSR from controller every time (except for the last bit). If
 * `response` is not NULL we store the response bytes in it. If the device does
 * not assert the DSR after a reasonable period of time the transfer is aborted.
 *
 * if `response` is not NULL it should be at least `length` bytes long.
 *
 * The return value is the number of bytes successfully exchanged (including the
 * last exchange that didn't set the DSR).
 */
static unsigned controller_send(uint16_t port,
                                unsigned length,
                                const uint8_t *to_send,
                                uint8_t *response) {
    unsigned i;

    /* TX enable, output selection down on the right port */
    write16(JOY_MC_BASE + 0xa, 0x3 | port);
    busy_loop(1000);

    for (i = 0; i < length; i++) {
        uint8_t r;

        /* If we're not at the first byte, we wait for DSR */
        if (i > 0) {
            /* As far as I know the longest possible time for the DSR to happen
             * is during byte 6 of a memory card read: it takes more than 30
             * thousand cycles to happen. For other exchanges it's usually a few
             * hundred cycles at most. */
            unsigned timeout = 100 * 1000;
            uint32_t stat = 0;

            while (timeout--) {
                stat = read32(JOY_MC_BASE + 0x4);
                if (stat & (1U << 7)) {
                    /* We have DSR */
                    break;
                }
            }

            if (!(stat & (1U << 7))) {
                /* No DSR pulse, abort. */
                break;
            }
        }

        /* Send byte */
        write8(JOY_MC_BASE + 0x0, to_send[i]);
        /* Wait for RX not empty, no timeout here because unless the hardware is
         * dysfunctional there shouldn't be any way that it ever occurs */
        while ((read32(JOY_MC_BASE + 0x4) & (1U << 1)) == 0) {
            ;
        }

        /* Read response */
        r = read8(JOY_MC_BASE + 0x0);
        if (response) {
            response[i] = r;
        }
    }

    /* De-select and unset TX enable */
    write16(JOY_MC_BASE + 0xa, 0);
    busy_loop(1000);

    return i;
}

int main(void) {
    uint8_t msg[10] = {0};
    uint8_t resp[10] = {0};
    unsigned rx_len;
    unsigned test_failed = 0;

#define CHECK(_pos, _expected) do { \
    if (_pos < rx_len && resp[_pos] != _expected) { \
        ERROR("Byte %u: expected 0x%02x got 0x%02x", \
                    _pos, _expected, resp[_pos]); \
        test_failed++; \
    }} while (0)

#define CHECK_NOT(_pos, _unexpected) do { \
    if (_pos < rx_len && resp[_pos] == _unexpected) { \
        ERROR("Byte %u has invalid value 0x%02x", \
                    _pos, _unexpected); \
        test_failed++; \
    }} while (0)

#define CHECK_LEN(_expected) do { \
    if (rx_len != _expected) { \
        ERROR("Command should return %u bytes, got %u", \
                    _expected, rx_len); \
        test_failed++; \
    } } while (0)

    controller_init();

    INFO("Switch controller to Dual Shock mode (43 00 01)");

    msg[0] = 0x01; /* Select controller */
    msg[1] = 0x43;
    msg[2] = 0x00;
    msg[3] = 0x01; /* 0x01: enter Dual Shock mode */
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    /* I always attempt to send more bytes than needed to see if the controller
     * sends spurious DSR pulses after the normal end of the transaction */
    rx_len = controller_send(PORT_1, 10, msg, resp);

    /* We receive 5 bytes if we were in digital mode, 9 in analog */
    if (rx_len != 5 && rx_len != 9) {
        ERROR("Couldn't switch controller to Dual Shock mode! [%x]", rx_len);
        return EXIT_FAILURE;
    }

    INFO("Unlock Rumble (4d)");

    msg[0] = 0x01;
    msg[1] = 0x4d;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x01;
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    /* This command returns the previously written values, since we don't know
     * them at this point we can't test them. */

    INFO("Unlock Rumble (4d)");

    msg[0] = 0x01;
    msg[1] = 0x4d;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x01;
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x01);
    CHECK(5, 0xff);
    CHECK(6, 0xff);
    CHECK(7, 0xff);
    CHECK(8, 0xff);

    INFO("Drive Rumble: small motor only (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x01; /* Small motor */
    msg[4] = 0x00; /* Big motor */
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    CHECK_NOT(5, 0xff);
    CHECK_NOT(5, 0);
    CHECK_NOT(6, 0xff);
    CHECK_NOT(6, 0);
    CHECK_NOT(7, 0xff);
    CHECK_NOT(7, 0);
    CHECK_NOT(8, 0xff);
    CHECK_NOT(8, 0);

    busy_loop(20000000);

    INFO("Drive Rumble: big motor only, 50%% (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x00; /* Small motor */
    msg[4] = 0x80; /* Big motor */
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    CHECK_NOT(5, 0xff);
    CHECK_NOT(5, 0);
    CHECK_NOT(6, 0xff);
    CHECK_NOT(6, 0);
    CHECK_NOT(7, 0xff);
    CHECK_NOT(7, 0);
    CHECK_NOT(8, 0xff);
    CHECK_NOT(8, 0);

    busy_loop(20000000);

    INFO("Drive Rumble: small motor only, 0xff (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0xff; /* Small motor: only bit 1 matters */
    msg[4] = 0x00; /* Big motor */
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    CHECK_NOT(5, 0xff);
    CHECK_NOT(5, 0);
    CHECK_NOT(6, 0xff);
    CHECK_NOT(6, 0);
    CHECK_NOT(7, 0xff);
    CHECK_NOT(7, 0);
    CHECK_NOT(8, 0xff);
    CHECK_NOT(8, 0);

    busy_loop(20000000);

    INFO("Drive Rumble: small motor off, 0xfe (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0xfe; /* Small motor: only bit 1 matters */
    msg[4] = 0x00; /* Big motor */
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    CHECK_NOT(5, 0xff);
    CHECK_NOT(5, 0);
    CHECK_NOT(6, 0xff);
    CHECK_NOT(6, 0);
    CHECK_NOT(7, 0xff);
    CHECK_NOT(7, 0);
    CHECK_NOT(8, 0xff);
    CHECK_NOT(8, 0);

    busy_loop(20000000);

    INFO("Unlock Rumble: small motor only (4d)");

    msg[0] = 0x01;
    msg[1] = 0x4d;
    msg[2] = 0x00;
    msg[3] = 0x00;
    /* 0x00: big motor disabled. It also appears to swap the motors in the
     * control? */
    msg[4] = 0x00;
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x01);
    CHECK(5, 0xff);
    CHECK(6, 0xff);
    CHECK(7, 0xff);
    CHECK(8, 0xff);

    INFO("Drive Rumble: small motor only, big motor disabled (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x80; /* Big motor, should be ignored */
    msg[4] = 0x81; /* Small motor */
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    CHECK_NOT(5, 0xff);
    CHECK_NOT(5, 0);
    CHECK_NOT(6, 0xff);
    CHECK_NOT(6, 0);
    CHECK_NOT(7, 0xff);
    CHECK_NOT(7, 0);
    CHECK_NOT(8, 0xff);
    CHECK_NOT(8, 0);

    busy_loop(20000000);

    INFO("Unlock Rumble: Enable both motors, inverted (4d)");

    msg[0] = 0x01;
    msg[1] = 0x4d;
    msg[2] = 0x00;
    msg[3] = 0x01;
    msg[4] = 0x00;
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x00);
    CHECK(4, 0x00);
    CHECK(5, 0xff);
    CHECK(6, 0xff);
    CHECK(7, 0xff);
    CHECK(8, 0xff);

    INFO("Drive Rumble: both motors, inverted (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x80; /* Big motor, should be ignored */
    msg[4] = 0x81; /* Small motor */
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    CHECK_NOT(5, 0xff);
    CHECK_NOT(5, 0);
    CHECK_NOT(6, 0xff);
    CHECK_NOT(6, 0);
    CHECK_NOT(7, 0xff);
    CHECK_NOT(7, 0);
    CHECK_NOT(8, 0xff);
    CHECK_NOT(8, 0);

    busy_loop(20000000);

    INFO("Unlock Rumble: Same command, should not stop the motors (4d)");

    msg[0] = 0x01;
    msg[1] = 0x4d;
    msg[2] = 0x00;
    msg[3] = 0x01;
    msg[4] = 0x00;
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x01);
    CHECK(4, 0x00);
    CHECK(5, 0xff);
    CHECK(6, 0xff);
    CHECK(7, 0xff);
    CHECK(8, 0xff);

    busy_loop(20000000);

    INFO("Lock Rumble: should not stop the motors (4d)");

    msg[0] = 0x01;
    msg[1] = 0x4d;
    msg[2] = 0x00;
    msg[3] = 0xff;
    msg[4] = 0xff;
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x01);
    CHECK(4, 0x00);
    CHECK(5, 0xff);
    CHECK(6, 0xff);
    CHECK(7, 0xff);
    CHECK(8, 0xff);

    busy_loop(20000000);

    INFO("Unlock Rumble: both motors, single command (4d)");

    msg[0] = 0x01;
    msg[1] = 0x4d;
    msg[2] = 0x00;
    msg[3] = 0x01;
    msg[4] = 0x01;
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x01);
    CHECK(4, 0x00);
    CHECK(5, 0xff);
    CHECK(6, 0xff);
    CHECK(7, 0xff);
    CHECK(8, 0xff);

    INFO("Drive Rumble: both motors, single command (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x81; /* Both motor, single command */
    msg[5] = 0xff;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    CHECK_NOT(5, 0xff);
    CHECK_NOT(5, 0);
    CHECK_NOT(6, 0xff);
    CHECK_NOT(6, 0);
    CHECK_NOT(7, 0xff);
    CHECK_NOT(7, 0);
    CHECK_NOT(8, 0xff);
    CHECK_NOT(8, 0);

    busy_loop(20000000);

    INFO("Unlock Rumble: both motors, big motor on byte 8 (4d)");

    msg[0] = 0x01;
    msg[1] = 0x4d;
    msg[2] = 0x00;
    msg[3] = 0x01;
    msg[4] = 0x01;
    msg[5] = 0x01;
    msg[6] = 0x01;
    msg[7] = 0x01;
    msg[8] = 0x01;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x01);
    CHECK(4, 0x01);
    CHECK(5, 0xff);
    CHECK(6, 0xff);
    CHECK(7, 0xff);
    CHECK(8, 0xff);

    INFO("Drive Rumble: both motors, big on byte 8 (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x01; /* small motor */
    msg[5] = 0x0;
    msg[6] = 0x0;
    msg[7] = 0x0;
    msg[8] = 0x80; /* Big Motor, for some reason */

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    CHECK_NOT(5, 0xff);
    CHECK_NOT(5, 0);
    CHECK_NOT(6, 0xff);
    CHECK_NOT(6, 0);
    CHECK_NOT(7, 0xff);
    CHECK_NOT(7, 0);
    CHECK_NOT(8, 0xff);
    CHECK_NOT(8, 0);

    busy_loop(20000000);

    INFO("Unlock Rumble: FFVIII sequence: (4d 00 ff 00 01 ff ff ff)");

    msg[0] = 0x01;
    msg[1] = 0x4d;
    msg[2] = 0x00;
    msg[3] = 0xff;
    msg[4] = 0x00;
    msg[5] = 0x01;
    msg[6] = 0xff;
    msg[7] = 0xff;
    msg[8] = 0xff;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0x01);
    CHECK(4, 0x01);
    CHECK(5, 0x01);
    CHECK(6, 0x01);
    CHECK(7, 0x01);
    CHECK(8, 0x01);

    INFO("Drive Rumble: small on byte 4, big on byte 5");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x01; /* Small motor */
    msg[5] = 0x80; /* Big motor */
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(9);
    CHECK(0, 0xff);
    CHECK(1, 0xf3);
    CHECK(2, 0x5a);
    CHECK(3, 0xff);
    CHECK(4, 0xff);
    CHECK_NOT(5, 0xff);
    CHECK_NOT(5, 0);
    CHECK_NOT(6, 0xff);
    CHECK_NOT(6, 0);
    CHECK_NOT(7, 0xff);
    CHECK_NOT(7, 0);
    CHECK_NOT(8, 0xff);
    CHECK_NOT(8, 0);

    busy_loop(20000000);

    INFO("Wait for watchdog to cut everything down");

    busy_loop(30000000);

    INFO("Read buttons (42)");

    msg[0] = 0x01;
    msg[1] = 0x42;
    msg[2] = 0x00;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = 0x00;
    msg[7] = 0x00;
    msg[8] = 0x00;

    rx_len = controller_send(PORT_1, 10, msg, resp);

    CHECK_LEN(5);
    CHECK(0, 0xff);
    CHECK(1, 0x41);
    CHECK(2, 0x5a);
    /* Assume no buttons are pressed for the sake of the test */
    CHECK(3, 0xff);
    CHECK(4, 0xff);

    if (test_failed) {
        bios_printf("%u test(s) failed\n", test_failed);
        return EXIT_FAILURE;
    } else {
        bios_printf("All tests passed\n");
        return EXIT_SUCCESS;
    }
}
