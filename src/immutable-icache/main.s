.include "common.s"

.global main
.type main, function

main:
    push $ra, $fp

    println "Running test.."

    # this pass writes the `nop` early, it should get cached.
    # $t0 should contain a 0.

.align 4

    li   $t0, 0     # pc = ~8
    la   $t1, 1f    # pc = ~c
                    # pc = ~0 !! NEXT CACHE LINE FILLED !!
    nop             # pc = ~4
    nop             # pc = ~8
    sw   $0, 0($t1) # pc = ~c
    nop             # pc = ~0 !! NEXT CACHE LINE FILLED !!
    nop             # pc = ~4
    nop             # pc = ~8
1:  li   $t0, 1     # pc = ~c
    bnez $t0, failed
    nop

    # this pass writes the `nop` too late, it shouldn't get cached.
    # $t0 should contain a 0.

.align 4

    li   $t0, 1     # pc = ~8
    la   $t1, 1f    # pc = ~c
                    # pc = ~0 !! NEXT CACHE LINE FILLED !!
    nop             # pc = ~4
    nop             # pc = ~8
    nop             # pc = ~c
    sw   $0, 0($t1) # pc = ~0 !! NEXT CACHE LINE FILLED !!
    nop             # pc = ~4
    nop             # pc = ~8
1:  li   $t0, 0     # pc = ~c
    bnez $t0, failed
    nop

passed:
    println "Test passed."

    pull $ra, $fp
    jr   $ra
    li   $v0, 0

failed:
    println "Test failed."

    pull $ra, $fp
    jr   $ra
    li   $v0, 1
