.include "common.s"

.global main
.type main, function

main:
    push $ra, $fp, $s0, $s1, $s2, $s3

    println "Running test.."
    println "Enabling only data-cache in the cache control register.."

    li $t0, 0x00000088
    li $t1, 0xfffe0130
    lw $s3, 0x0($t1)
    sw $t0, 0x0($t1)

    println "Enabling ISC, filling the data-cache with incrementing values.."

    li   $t0, 0x10000
    mtc0 $t0, $12
    nop
    nop

    li   $s1, (0x100000 + 0x3fc)
    li   $s0, 0x3fc
1:  sw   $s0, 0x0($s1)
    addi $s1,-0x4
    bne  $s0, $zero, 1b
    addi $s0,-0x4

    li   $t0, 0x0
    mtc0 $t0, $12
    nop
    nop

    sw   $s3, 0x0($t1)

    println "ISC disabled, verifying that incrementing values have appeared at 0x1f800000.."

    li   $s1, 0x1f8003fc
    li   $s0, 0x3fc
1:  lw   $s2, 0x0($s1)
    nop
    bne  $s0, $s2, failed
    nop
    addi $s1,-0x4
    bnez $s0, 1b
    addi $s0,-0x4

passed:
    println "Test passed."

    pull $ra, $fp, $s0, $s1, $s2, $s3
    jr $ra
    li $v0, 0

failed:
    println "Test failed."

    pull $ra, $fp, $s0, $s1, $s2, $s3
    jr $ra
    li $v0, 1
