#include "psx.h"
#include "test.h"

static const uint32_t REG_AND[32] = {
  0x00FFFFFF, 0xFFFFFFFF, 0x71770703, 0x71770703, // DMA0
  0x00FFFFFF, 0xFFFFFFFF, 0x71770703, 0x71770703, // DMA1
  0x00FFFFFF, 0xFFFFFFFF, 0x71770703, 0x71770703, // DMA2
  0x00FFFFFF, 0xFFFFFFFF, 0x71770703, 0x71770703, // DMA3
  0x00FFFFFF, 0xFFFFFFFF, 0x71770703, 0x71770703, // DMA4
  0x00FFFFFF, 0xFFFFFFFF, 0x71770703, 0x71770703, // DMA5
  0x00FFFFFF, 0xFFFFFFFF, 0x51000000, 0x51000000, // DMA6
  0xFFFFFFFF, 0x80FF803F, 0x00000000, 0x00000000, // CTRL
};

static const uint32_t REG_OR[32] = {
  0x00000000, 0x00000000, 0x00000000, 0x00000000, // DMA0
  0x00000000, 0x00000000, 0x00000000, 0x00000000, // DMA1
  0x00000000, 0x00000000, 0x00000000, 0x00000000, // DMA2
  0x00000000, 0x00000000, 0x00000000, 0x00000000, // DMA3
  0x00000000, 0x00000000, 0x00000000, 0x00000000, // DMA4
  0x00000000, 0x00000000, 0x00000000, 0x00000000, // DMA5
  0x00000000, 0x00000000, 0x00000002, 0x00000002, // DMA6
  0x00000000, 0x00000000, 0x00000000, 0x00000000, // CTRL
};

void test_mmio_register(uint32_t address, uint32_t data) {
  INFO("Testing I/O address 0x%08x", address);

  write32(address, data);

  data = data & REG_AND[(address / 4) & 0x1f];
  data = data | REG_OR[(address / 4) & 0x1f];

  do {
    int expected = data;
    int actual = read32(address);
    ASSERT_EQ(expected, actual, "32-bit read is wrong. Expected %08x, but got %08x.");
  } while(0);

  for (int i = 0; i < 4; i += 2) {
    int expected = (data >> (i * 8)) & 0xffff;
    int actual = read16(address + i);
    ASSERT_EQ(expected, actual, "16-bit read is wrong. Expected %08x, but got %08x.");
  }

  for (int i = 0; i < 4; i += 1) {
    int expected = (data >> (i * 8)) & 0xff;
    int actual = read8(address + i);
    ASSERT_EQ(expected, actual, "8-bit read is wrong. Expected %08x, but got %08x.");
  }
}

static void test_write32_read() {
  INFO("Testing basic bit masking behavior.");

  for (int channel = 0x00; channel < 0x70; channel += 0x10) {
    test_mmio_register(0x1F801080 + channel, 0xFFFFFFFF);
    test_mmio_register(0x1F801084 + channel, 0xFFFFFFFF);
    test_mmio_register(0x1F801088 + channel, 0xEEFFFFFF);
    test_mmio_register(0x1F80108C + channel, 0xEEFFFFFF); // Mirror of 0x1F8010x8
  }

  test_mmio_register(0x1F8010F0, 0xFFFFFFFF);
  test_mmio_register(0x1F8010F4, 0xFFFFFFFF);
}

int main() {
  test_write32_read();

  INFO("All tests complete.");
  return 0;
}
