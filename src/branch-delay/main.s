.include "common.s"

.global main
.type main, function

main:
    push $ra, $fp

    println "Running test.."

    li   $t0, 1
    b    1f
    addi $t0,-1 # this executes even though we branched passed it.
    addi $t0,-1 # this doesn't, however.
1:  bnez $t0, failed
    nop

    li   $t0, 1
1:  bnez $t0, 1b
    andi $t0, 0 # this also occurs with backwards branching.
    bnez $t0, failed
    nop

    # branching to what would be the next instruction causes that instruction
    # to get executed twice. once in the delay slot, and once as the target of
    # the branch.

    li   $t0, 2
    b    1f
1:  addi $t0,-1
    bnez $t0, failed
    nop

passed:
    println "Test passed."

    pull $ra, $fp
    jr   $ra
    li   $v0, 0

failed:
    println "Test failed."

    pull $ra, $fp
    jr   $ra
    li   $v0, 1
