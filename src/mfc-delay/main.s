.include "common.s"

.global main
.type main, function

main:
    push $ra, $fp

    println "Running test.."

    li   $t0, 0x12345678
    move $t1, $t0

    mfc0 $t1, $12
    move $t2, $t1 # value should be the same as $t0
    bne  $t0, $t2, failed
    nop

passed:
    println "Test passed."

    pull $ra, $fp
    jr   $ra
    li   $v0, 0

failed:
    println "Test failed."

    pull $ra, $fp
    jr   $ra
    li   $v0, 1
