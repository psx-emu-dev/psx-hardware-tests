#include "psx.h"
#include "test.h"

static uint32_t read_data() {
  return read32(MDEC_BASE + 0);
}

static uint32_t read_stat() {
  return read32(MDEC_BASE + 4);
}

static void write_command(uint32_t val) {
  write32(MDEC_BASE + 0, val);
}

static void write_control(uint32_t val) {
  write32(MDEC_BASE + 4, val);
}

TEST(command_1_words_remaining, {
  uint32_t status;

  write_control(0xE0000000);
  write_command(0x20000000);

  status = read_stat() & 0xFFFF;
  TEST_ASSERT_EQ(0xFFFF, status,
    "Status is wrong after sending command. Expected %04x, but got %04x.");

  for (unsigned int i = 1; i < 65536; i++) {
    while ((read_stat() & (1 << 31)) == 0) { read_data(); }
    while ((read_stat() & (1 << 30)) != 0) {}

    write_command(0);
    status = read_stat() & 0xFFFF;
    TEST_ASSERT_EQ((0xFFFF - i), status,
      "Status is wrong after sending parameter. Expected %04x, but got %04x.");
  }

  write_command(0);
  status = read_stat() & 0xFFFF;
  TEST_ASSERT_EQ(0xFFFF, status,
    "Status is wrong after sending all parameters. Expected %04x, but got %04x.");
})

TEST(command_2_words_remaining, {
  uint32_t status;

  write_control(0xE0000000);
  write_command(0x40000000);

  status = read_stat();
  TEST_ASSERT_EQ(0xa004000f, status,
    "Status is wrong after sending command. Expected %08x, but got %08x.");

  for (int i = 1; i < 16; i++) {
    write_command(0);
    status = read_stat();
    TEST_ASSERT_EQ(0xa0040000 | (15 - i), status,
      "Status is wrong after sending parameter. Expected %08x, but got %08x.");
  }

  write_command(0);

  while (read_stat() & (1 << 29)) ;

  status = read_stat();
  TEST_ASSERT_EQ(0x8004ffff, status,
    "Status is wrong after sending all parameters. Expected %08x, but got %08x.");
})

TEST(command_3_words_remaining, {
  uint32_t status;

  write_control(0xE0000000);
  write_command(0x60000000);

  status = read_stat();
  TEST_ASSERT_EQ(0xA004001F, status,
    "Status is wrong after sending command. Expected %08x, but got %08x.");

  for (int i = 0; i < 31; i++) {
    write_command(0);
    status = read_stat();
    TEST_ASSERT_EQ(0xA0040000 | (30 - i), status,
      "Status is wrong after sending parameter. Expected %08x, but got %08x.");
  }

  write_command(0);

  while (read_stat() & (1 << 29)) ;

  status = read_stat();
  TEST_ASSERT_EQ(0x8004FFFF, status,
    "Status is wrong after sending all parameters. Expected %08x, but got %08x.");
})

TEST(command_1_less_than_complete_block, {
  INFO("stat=%08x", read_stat());

  write_command(0x20000010);

  INFO("stat=%08x", read_stat());

  for (int i = 0; i < 16; i++) {
    write_command(0);
    INFO("stat=%08x", read_stat());
  }

  write_command(0);
  INFO("stat=%08x", read_stat());
})

int main() {
  bool result =
    __test_command_1_words_remaining() &&
    __test_command_2_words_remaining() &&
    __test_command_3_words_remaining() &&
    __test_command_1_less_than_complete_block();

  return result ? EXIT_SUCCESS : EXIT_FAILURE;
}
