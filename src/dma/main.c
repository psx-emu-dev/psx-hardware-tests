#include "psx.h"
#include "test.h"

// Enough data to fill 4 MDEC(3) commands.
uint32_t dma_data[128];

TEST(dma0_drq_block_32x2, {
  uint32_t source = ((uint32_t) &dma_data[0]) & 0x00ffffff;
  uint32_t madr;
  uint32_t bcr;
  uint32_t chcr;

  /* Initialize MDEC */

  write32(0x1F801824, 0x80000000); // Reset MDEC to a known state
  write32(0x1F801824, 0x40000000); // Enable DMA0 DRQ

  /* Initialize DMA0 */

  write32(0x1F801080, source); // Set the source address
  write32(0x1F801084, 0x00020020); // Block size=32, Blocks=2
  write32(0x1F801088, 0x01000201); // RAM->MDEC, Forward, Enabled

  /* Assert basics */

  madr = read32(0x1F801080);
  TEST_ASSERT_EQ(source, madr, "MADR is wrong after initializing. Expected %08x, but got %08x.");

  bcr = read32(0x1F801084);
  TEST_ASSERT_EQ(0x00020020, bcr, "BCR is wrong after initializing. Expected %08x, but got %08x.");

  chcr = read32(0x1F801088);
  TEST_ASSERT_EQ(0x01000201, chcr, "CHCR is wrong after initializing. Expected %08x, but got %08x.");

  /* Send a MDEC(3) command, this should transfer 32 words (128 bytes) immediately */

  write32(0x1F801820, 0x60000000);

  busy_loop(10); // Seems to take a few cycles to actually initiate

  madr = read32(0x1F801080);
  TEST_ASSERT_EQ(source + 128, madr, "MADR is wrong after 1 MDEC(3) command. Expected %08x, but got %08x.");

  bcr = read32(0x1F801084);
  TEST_ASSERT_EQ(0x00010020, bcr, "BCR is wrong after 1 MDEC(3) command. Expected %08x, but got %08x.");

  chcr = read32(0x1F801088);
  TEST_ASSERT_EQ(0x01000201, chcr, "CHCR is wrong after 1 MDEC(3) command. Expected %08x, but got %08x.");

  /* Send another MDEC(3) command, this should transfer 32 more words (128 bytes) and end the DMA */

  write32(0x1F801820, 0x60000000);

  busy_loop(10); // Seems to take a few cycles to actually initiate

  madr = read32(0x1F801080);
  TEST_ASSERT_EQ(source + 256, madr, "MADR is wrong after 2 MDEC(3) commands. Expected %08x, but got %08x.");

  bcr = read32(0x1F801084);
  TEST_ASSERT_EQ(0x00000020, bcr, "BCR is wrong after 2 MDEC(3) commands. Expected %08x, but got %08x.");

  chcr = read32(0x1F801088);
  TEST_ASSERT_EQ(0x00000201, chcr, "CHCR is wrong after 2 MDEC(3) commands. Expected %08x, but got %08x.");

  /* Send another MDEC(3) command, this should have no effect since the DMA has finished */

  write32(0x1F801820, 0x60000000);

  busy_loop(10); // Seems to take a few cycles to actually initiate

  madr = read32(0x1F801080);
  TEST_ASSERT_EQ(source + 256, madr, "MADR is wrong after 2 MDEC(3) commands. Expected %08x, but got %08x.");

  bcr = read32(0x1F801084);
  TEST_ASSERT_EQ(0x00000020, bcr, "BCR is wrong after 2 MDEC(3) commands. Expected %08x, but got %08x.");

  chcr = read32(0x1F801088);
  TEST_ASSERT_EQ(0x00000201, chcr, "CHCR is wrong after 2 MDEC(3) commands. Expected %08x, but got %08x.");
})

TEST(dma0_drq_block_16x4, {
  uint32_t source = ((uint32_t) &dma_data[0]) & 0x00ffffff;
  uint32_t madr;
  uint32_t bcr;
  uint32_t chcr;

  /* Initialize MDEC */

  write32(0x1F801824, 0x80000000); // Reset MDEC to a known state
  write32(0x1F801824, 0x40000000); // Enable DMA0 DRQ

  /* Initialize DMA0 */

  write32(0x1F801080, source); // Set the source address
  write32(0x1F801084, 0x00040010); // Block size=16, Blocks=4
  write32(0x1F801088, 0x01000201); // RAM->MDEC, Forward, Enabled

  /* Assert basics */

  madr = read32(0x1F801080);
  TEST_ASSERT_EQ(source, madr, "MADR is wrong after initializing. Expected %08x, but got %08x.");

  bcr = read32(0x1F801084);
  TEST_ASSERT_EQ(0x00040010, bcr, "BCR is wrong after initializing. Expected %08x, but got %08x.");

  chcr = read32(0x1F801088);
  TEST_ASSERT_EQ(0x01000201, chcr, "CHCR is wrong after initializing. Expected %08x, but got %08x.");

  /* Send a MDEC(3) command, this should transfer 32 words (128 bytes) immediately */

  write32(0x1F801820, 0x60000000);

  busy_loop(10); // Seems to take a few cycles to actually initiate

  madr = read32(0x1F801080);
  TEST_ASSERT_EQ(source + 128, madr, "MADR is wrong after 1 MDEC(3) command. Expected %08x, but got %08x.");

  bcr = read32(0x1F801084);
  TEST_ASSERT_EQ(0x00020010, bcr, "BCR is wrong after 1 MDEC(3) command. Expected %08x, but got %08x.");

  chcr = read32(0x1F801088);
  TEST_ASSERT_EQ(0x01000201, chcr, "CHCR is wrong after 1 MDEC(3) command. Expected %08x, but got %08x.");

  /* Send another MDEC(3) command, this should transfer 32 more words (128 bytes) and end the DMA */

  write32(0x1F801820, 0x60000000);

  busy_loop(10); // Seems to take a few cycles to actually initiate

  madr = read32(0x1F801080);
  TEST_ASSERT_EQ(source + 256, madr, "MADR is wrong after 2 MDEC(3) commands. Expected %08x, but got %08x.");

  bcr = read32(0x1F801084);
  TEST_ASSERT_EQ(0x00000010, bcr, "BCR is wrong after 2 MDEC(3) commands. Expected %08x, but got %08x.");

  chcr = read32(0x1F801088);
  TEST_ASSERT_EQ(0x00000201, chcr, "CHCR is wrong after 2 MDEC(3) commands. Expected %08x, but got %08x.");

  /* Send another MDEC(3) command, this should have no effect since the DMA has finished */

  write32(0x1F801820, 0x60000000);

  busy_loop(10); // Seems to take a few cycles to actually initiate

  madr = read32(0x1F801080);
  TEST_ASSERT_EQ(source + 256, madr, "MADR is wrong after 2 MDEC(3) commands. Expected %08x, but got %08x.");

  bcr = read32(0x1F801084);
  TEST_ASSERT_EQ(0x00000010, bcr, "BCR is wrong after 2 MDEC(3) commands. Expected %08x, but got %08x.");

  chcr = read32(0x1F801088);
  TEST_ASSERT_EQ(0x00000201, chcr, "CHCR is wrong after 2 MDEC(3) commands. Expected %08x, but got %08x.");
})

TEST(dma0_drq_block_31x2, {
  uint32_t source = ((uint32_t) &dma_data[0]) & 0x00ffffff;
  uint32_t madr;
  uint32_t bcr;
  uint32_t chcr;

  /* Initialize MDEC */

  write32(0x1F801824, 0x80000000); // Reset MDEC to a known state
  write32(0x1F801824, 0x40000000); // Enable DMA0 DRQ

  /* Initialize DMA0 */

  write32(0x1F801080, source); // Set the source address
  write32(0x1F801084, 0x0002001f); // Block size=31, Blocks=2
  write32(0x1F801088, 0x01000201); // RAM->MDEC, Forward, Enabled

  /* Assert basics */

  madr = read32(0x1F801080);
  TEST_ASSERT_EQ(source, madr, "MADR is wrong after initializing. Expected %08x, but got %08x.");

  bcr = read32(0x1F801084);
  TEST_ASSERT_EQ(0x0002001f, bcr, "BCR is wrong after initializing. Expected %08x, but got %08x.");

  chcr = read32(0x1F801088);
  TEST_ASSERT_EQ(0x01000201, chcr, "CHCR is wrong after initializing. Expected %08x, but got %08x.");

  /* Send a MDEC(3) command, this should transfer 62 words (248 bytes) immediately */

  write32(0x1F801820, 0x60000000);

  busy_loop(10); // Seems to take a few cycles to actually initiate

  madr = read32(0x1F801080);
  TEST_ASSERT_EQ(source + 248, madr, "MADR is wrong after 1 MDEC(3) command. Expected %08x, but got %08x.");

  bcr = read32(0x1F801084);
  TEST_ASSERT_EQ(0x0000001f, bcr, "BCR is wrong after 1 MDEC(3) command. Expected %08x, but got %08x.");

  chcr = read32(0x1F801088);
  TEST_ASSERT_EQ(0x00000201, chcr, "CHCR is wrong after 1 MDEC(3) command. Expected %08x, but got %08x.");

  /* Send another MDEC(3) command, this should have no effect since the DMA has finished */

  write32(0x1F801820, 0x60000000);

  busy_loop(10); // Seems to take a few cycles to actually initiate

  madr = read32(0x1F801080);
  TEST_ASSERT_EQ(source + 248, madr, "MADR is wrong after 2 MDEC(3) commands. Expected %08x, but got %08x.");

  bcr = read32(0x1F801084);
  TEST_ASSERT_EQ(0x0000001f, bcr, "BCR is wrong after 2 MDEC(3) commands. Expected %08x, but got %08x.");

  chcr = read32(0x1F801088);
  TEST_ASSERT_EQ(0x00000201, chcr, "CHCR is wrong after 2 MDEC(3) commands. Expected %08x, but got %08x.");
})

int main() {
  /**
   * When the MDEC FIFO is overrun, a semi-random parameter gets interpreted as
   * a command instead. In my playing it seemed to be the 27th word after the
   * command starts. (32+27=59)
   **/

  dma_data[59] = 0x2000cafe;

  bool result =
      __test_dma0_drq_block_32x2() &&
      __test_dma0_drq_block_16x4() &&
      __test_dma0_drq_block_31x2();

  return result ? EXIT_SUCCESS : EXIT_FAILURE;
}
