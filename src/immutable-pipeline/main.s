.include "common.s"
.include "segment.s"

.global main
.type main, function

main:
    push $ra, $fp

    println "Running test.."

    jal enter_kseg1
    nop

    # this pass writes the `nop` early, the pipeline should load the `nop`.
    # $t0 should contain a 0.

    li   $t0, 0
    la   $t1, 1f
    sw   $0, 0($t1)
    nop
    nop
1:  li   $t0, 1
    bnez $t0, failed
    nop

    # this pass writes the `nop` too late, the pipeline should load the `li`.
    # $t0 should contain a 0.

    li   $t0, 1
    la   $t1, 1f
    sw   $0, 0($t1)
    nop
1:  li   $t0, 0
    bnez $t0, failed
    nop

passed:
    println "Test passed."

    pull $ra, $fp
    jr   $ra
    li   $v0, 0

failed:
    println "Test failed."

    pull $ra, $fp
    jr   $ra
    li   $v0, 1
