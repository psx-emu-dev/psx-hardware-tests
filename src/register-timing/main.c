#include "bios.h"
#include "memory.h"
#include "utils.h"
#include "timer.h"

#define VOICE_0_VOLUME 0x1F801C00

static void __attribute__ ((noinline)) run_test() {
  timer_0_init();
  uint32_t start32 = timer_0_value();
  write32(VOICE_0_VOLUME, 0);
  uint32_t elapsed32 = timer_0_value() - start32;

  timer_0_init();
  uint32_t start16 = timer_0_value();
  write16(VOICE_0_VOLUME, 0);
  uint32_t elapsed16 = timer_0_value() - start16;

  timer_0_init();
  uint32_t start8 = timer_0_value();
  write8(VOICE_0_VOLUME, 0);
  uint32_t elapsed8 = timer_0_value() - start8;

  bios_printf("32-bit write: %d\n", elapsed32);
  bios_printf("16-bit write: %d\n", elapsed16);
  bios_printf("8-bit write: %d\n", elapsed8);
}

int main() {
  enter_kseg1();

  run_test(); // for caching purposes
  run_test();

  return 0;
}
